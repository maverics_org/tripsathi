<!DOCTYPE html>
<html lang="en">
<head>
        <title>Tripsathi | cruises-detail</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome/css/font-awesome.css">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-flaticon/flaticon.css">
        <!-- LIBRARY CSS-->
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/animate/animate.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick-theme.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/selectbox/css/jquery.selectbox.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/please-wait/please-wait.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox8cbb.css?v=2.1.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-buttons3447.css?v=1.0.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-thumbsf2ad.css?v=1.0.7">
        <!-- STYLE CSS-->
        <link type="text/css" rel="stylesheet" href="assets/css/layout.css">
        <link type="text/css" rel="stylesheet" href="assets/css/components.css">
        <link type="text/css" rel="stylesheet" href="assets/css/responsive.css">
        <link type="text/css" rel="stylesheet" href="assets/css/color.css">
        <!--link(type="text/css", rel='stylesheet', href='assets/css/color-1/color-1.css', id="color-skins")-->
        <link type="text/css" rel="stylesheet" href="#" id="color-skins">
        <script src="assets/libs/jquery/jquery-2.2.3.min.js"></script>
        <script src="assets/libs/js-cookie/js.cookie.js"></script>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/' + Cookies.get('color-skin') + '/' + 'color.css');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/color-1/color.css');
            }
        </script>
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
    </head>
    <body>
        <div class="body-wrapper">
            <!-- MENU MOBILE-->
            
            <?php include('mobile-menu.php');?><!-- WRAPPER CONTENT-->
            <div class="wrapper-content">
                <!-- HEADER-->
                
                <?php include('header-2.php');?><!-- WRAPPER-->
                <div id="wrapper-content">
                    <!-- MAIN CONTENT-->
                    <div class="main-content">
                        <section class="page-banner cruises-detail">
                            <div class="container">
                                <div class="page-title-wrapper">
                                    <div class="page-title-content">
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="index.html" class="link home">Home</a>
                                            </li>
                                            <li class="active">
                                                <a href="#" class="link">cruises</a>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <h2 class="captions">cruises detail</h2>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="page-main">
                            <div class="trip-info">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="label-route-widget">
                                                <span class="departure">
                                                    <span class="city">Singapore, </span>
                                                    <span class="country">Singapore</span>
                                                </span>
                                                <i class="fa fa-long-arrow-right"></i>
                                                <span class="arrival">
                                                    <span class="city">Kuala Lumpur, </span>
                                                    <span class="country">Malaysia</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="label-time-widget">From
                                                <span class="departure">
                                                    <span class="date">6 March </span>at
                                                    <span class="hour">10:00</span>
                                                </span> to
                                                <span class="arrival">
                                                    <span class="date">9 March </span>at
                                                    <span class="hour">10:00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="cruises-result-detail padding-top padding-bottom">
                                <div class="container">
                                    <div class="result-body">
                                        <div class="row">
                                            <div class="col-md-8 col-xs-12 main-right">
                                                <div class="warpper-slider-detail">
                                                    <div class="wrapper-cd-detail">
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-1.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-2.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-3.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-4.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-5.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-6.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-7.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-8.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-9.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="item-cd">
                                                            <a href="#">
                                                                <img src="assets/images/cruises-detail/cruises-detail-10.jpg" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="wrapper-cd-detail-thumnail">
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-1.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-2.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-3.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-4.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-5.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-6.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-7.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-8.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-9.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="thumnail-item">
                                                            <img src="assets/images/cruises-detail/cruises-detail-10.jpg" alt="" class="img-responsive">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wrapper-timeline">
                                                    <h3 class="title-style-3">Tour Overview</h3>
                                                    <div class="car-rent-layout">
                                                        <div class="content-wrapper">
                                                            <a href="#" class="title">ANTALYA</a>
                                                            <div class="price">
                                                                <sup>$</sup>
                                                                <span class="number">250</span>
                                                                <p class="for-price">per day</p>
                                                            </div>
                                                            <div class="text">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</div>
                                                            <div
                                                                class="text">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus. Lorem ipsum
                                                                dolor sit amet, consectetur elit dolor sit amet. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</div>
                                                    </div>
                                                </div>
                                                <div class="cruises-detail-tag tags-widget">
                                                    <span class="content-tag">Room Types:</span>
                                                    <div class="content-widget">
                                                        <a href="javascript:void(0)" class="tag">President Suite</a>
                                                        <a href="javascript:void(0)" class="tag">Super Deluxe</a>
                                                        <a href="javascript:void(0)" class="tag">Luxury</a>
                                                        <a href="javascript:void(0)" class="tag">Economy</a>
                                                    </div>
                                                </div>
                                                <div class="timeline-container">
                                                    <div class="timeline">
                                                        <div class="timeline-block">
                                                            <div class="timeline-title">
                                                                <span>Day 01</span>
                                                            </div>
                                                            <div class="timeline-content medium-margin-top">
                                                                <div class="row">
                                                                    <div class="timeline-point">
                                                                        <i class="fa fa-circle-o"></i>
                                                                    </div>
                                                                    <div class="timeline-custom-col content-col">
                                                                        <div class="timeline-location-block">
                                                                            <p class="location-name">London England
                                                                                <i class="fa fa-map-marker icon-marker"></i>
                                                                            </p>
                                                                            <p class="description">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices
                                                                                purus. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="timeline-block">
                                                            <div class="timeline-title">
                                                                <span>Day 02</span>
                                                            </div>
                                                            <div class="timeline-content medium-margin-top">
                                                                <div class="row">
                                                                    <div class="timeline-point">
                                                                        <i class="fa fa-circle-o"></i>
                                                                    </div>
                                                                    <div class="timeline-custom-col content-col">
                                                                        <div class="timeline-location-block">
                                                                            <p class="location-name">Nottingham England
                                                                                <i class="fa fa-map-marker icon-marker"></i>
                                                                            </p>
                                                                            <p class="description">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices
                                                                                purus. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="timeline-block">
                                                            <div class="timeline-title">
                                                                <span>Day 03</span>
                                                            </div>
                                                            <div class="timeline-content medium-margin-top">
                                                                <div class="row">
                                                                    <div class="timeline-point">
                                                                        <i class="fa fa-circle-o"></i>
                                                                    </div>
                                                                    <div class="timeline-custom-col content-col">
                                                                        <div class="timeline-location-block">
                                                                            <p class="location-name">Birmingham England
                                                                                <i class="fa fa-map-marker icon-marker"></i>
                                                                            </p>
                                                                            <p class="description">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices
                                                                                purus. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="timeline-block">
                                                            <div class="timeline-title">
                                                                <span>Day 04</span>
                                                            </div>
                                                            <div class="timeline-content medium-margin-top">
                                                                <div class="row">
                                                                    <div class="timeline-point">
                                                                        <i class="fa fa-circle-o"></i>
                                                                    </div>
                                                                    <div class="timeline-custom-col content-col">
                                                                        <div class="timeline-location-block">
                                                                            <p class="location-name">Manchester England
                                                                                <i class="fa fa-map-marker icon-marker"></i>
                                                                            </p>
                                                                            <p class="description">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices
                                                                                purus. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wrapper-btn margin-top70">
                                                        <a href="#" class="btn btn-maincolor btn-book-tour">book now</a>
                                                    </div>
                                                    <div class="timeline-book-block book-tour">
                                                        <div class="find-widget find-hotel-widget widget new-style">
                                                            <h4 class="title-widgets">BOOK ROOM</h4>
                                                            <form class="content-widget">
                                                                <div class="text-input small-margin-top">
                                                                    <div class="input-daterange">
                                                                        <div class="text-box-wrapper half">
                                                                            <label class="tb-label">Check in</label>
                                                                            <div class="input-group">
                                                                                <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                                <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-box-wrapper half">
                                                                            <label class="tb-label">Check out</label>
                                                                            <div class="input-group">
                                                                                <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                                <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="count adult-count text-box-wrapper">
                                                                        <label class="tb-label">Adult</label>
                                                                        <div class="select-wrapper">
                                                                            <!--i.fa.fa-chevron-down-->
                                                                            <select class="form-control custom-select selectbox">
                                                                                <option selected="selected">1</option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                                <option>6</option>
                                                                                <option>7</option>
                                                                                <option>8</option>
                                                                                <option>9</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="count child-count text-box-wrapper">
                                                                        <label class="tb-label">Child</label>
                                                                        <div class="select-wrapper">
                                                                            <!--i.fa.fa-chevron-down-->
                                                                            <select class="form-control custom-select selectbox">
                                                                                <option selected="selected">0</option>
                                                                                <option>1</option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                                <option>6</option>
                                                                                <option>7</option>
                                                                                <option>8</option>
                                                                                <option>9</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="first-name text-box-wrapper">
                                                                        <label class="tb-label">Your First Name</label>
                                                                        <div class="input-group">
                                                                            <input type="text" placeholder="Write your first name" class="tb-input">
                                                                        </div>
                                                                    </div>
                                                                    <div class="last-name text-box-wrapper">
                                                                        <label class="tb-label">Your Last Name</label>
                                                                        <div class="input-group">
                                                                            <input type="text" placeholder="Write your last name" class="tb-input">
                                                                        </div>
                                                                    </div>
                                                                    <div class="email text-box-wrapper">
                                                                        <label class="tb-label">Your Email</label>
                                                                        <div class="input-group">
                                                                            <input type="email" placeholder="Write your email address" class="tb-input">
                                                                        </div>
                                                                    </div>
                                                                    <div class="phone text-box-wrapper">
                                                                        <label class="tb-label">Your Number Phone</label>
                                                                        <div class="input-group">
                                                                            <input type="text" placeholder="Write your number phone" class="tb-input">
                                                                        </div>
                                                                    </div>
                                                                    <div class="place text-box-wrapper">
                                                                        <label class="tb-label">Where are your address?</label>
                                                                        <div class="input-group">
                                                                            <input type="text" placeholder="Write your address" class="tb-input">
                                                                        </div>
                                                                    </div>
                                                                    <div class="note text-box-wrapper">
                                                                        <label class="tb-label">Note:</label>
                                                                        <div class="input-group">
                                                                            <textarea placeholder="Write your note" rows="3" name="content" class="tb-input"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <button type="submit" data-hover="SEND REQUEST" class="btn btn-slide">
                                                                        <span class="text">BOOK Now</span>
                                                                        <span class="icons fa fa-long-arrow-right"></span>
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 sidebar-widget">
                                            <div class="col-2">
                                                <div class="find-widget find-flight-widget widget">
                                                    <h4 class="title-widgets">find your cruises</h4>
                                                    <form class="content-widget">
                                                        <div class="ffw-radio-selection">
                                                            <span class="ffw-radio-btn-wrapper">
                                                                <input type="radio" name="flight type" value="one way" id="flight-type-1" checked="checked" class="ffw-radio-btn">
                                                                <label for="flight-type-1" class="ffw-radio-label">One Way</label>
                                                            </span>
                                                            <span class="ffw-radio-btn-wrapper">
                                                                <input type="radio" name="flight type" value="round trip" id="flight-type-2" class="ffw-radio-btn">
                                                                <label for="flight-type-2" class="ffw-radio-label">Round Trip</label>
                                                            </span>
                                                            <span class="ffw-radio-btn-wrapper">
                                                                <input type="radio" name="flight type" value="multiple cities" id="flight-type-3" class="ffw-radio-btn">
                                                                <label for="flight-type-3" class="ffw-radio-label">Multiple Cities</label>
                                                            </span>
                                                            <div class="stretch">&nbsp;</div>
                                                        </div>
                                                        <div class="text-input small-margin-top">
                                                            <div class="text-box-wrapper">
                                                                <label class="tb-label">Where do you want to go?</label>
                                                                <div class="input-group">
                                                                    <input type="text" placeholder="Write the place" class="tb-input">
                                                                </div>
                                                            </div>
                                                            <div class="input-daterange">
                                                                <div class="text-box-wrapper half left">
                                                                    <label class="tb-label">Check in</label>
                                                                    <div class="input-group">
                                                                        <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                        <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="text-box-wrapper half right">
                                                                    <label class="tb-label">Check out</label>
                                                                    <div class="input-group">
                                                                        <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                        <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="text-box-wrapper half left">
                                                                <label class="tb-label">Number of Adult</label>
                                                                <div class="input-group">
                                                                    <button disabled="disabled" data-type="minus" data-field="quant[1]" class="input-group-btn btn-minus">
                                                                        <span class="tb-icon fa fa-minus"></span>
                                                                    </button>
                                                                    <input type="number" name="quant[1]" min="1" max="9" value="1" class="tb-input count">
                                                                    <button data-type="plus" data-field="quant[1]" class="input-group-btn btn-plus">
                                                                        <span class="tb-icon fa fa-plus"></span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="text-box-wrapper half right">
                                                                <label class="tb-label">Number of Child</label>
                                                                <div class="input-group">
                                                                    <button disabled="disabled" data-type="minus" data-field="quant[2]" class="input-group-btn btn-minus">
                                                                        <span class="tb-icon fa fa-minus"></span>
                                                                    </button>
                                                                    <input type="number" name="quant[2]" min="0" max="9" value="0" class="tb-input count">
                                                                    <button data-type="plus" data-field="quant[2]" class="input-group-btn btn-plus">
                                                                        <span class="tb-icon fa fa-plus"></span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" data-hover="SEND NOW" class="btn btn-slide small-margin-top">
                                                            <span class="text">search now</span>
                                                            <span class="icons fa fa-long-arrow-right"></span>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="col-1">
                                                    <div class="price-widget widget">
                                                        <div class="title-widget">
                                                            <div class="title">price</div>
                                                        </div>
                                                        <div class="content-widget">
                                                            <div class="price-wrapper">
                                                                <div data-range_min="0" data-range_max="3000" data-cur_min="450" data-cur_max="1800" class="nstSlider">
                                                                    <div class="leftGrip indicator">
                                                                        <div class="number"></div>
                                                                    </div>
                                                                    <div class="rightGrip indicator">
                                                                        <div class="number"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="leftLabel">0</div>
                                                                <div class="rightLabel">3000</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="turkey-cities-widget widget">
                                                        <div class="title-widget">
                                                            <div class="title">rating</div>
                                                        </div>
                                                        <div class="content-widget">
                                                            <form class="radio-selection">
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="rating" value="1stars" id="1stars" class="radio-btn">
                                                                    <label for="1stars" class="radio-label stars stars5">1stars</label>
                                                                    <span class="count">27</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="rating" value="2stars" id="2stars" class="radio-btn">
                                                                    <label for="2stars" class="radio-label stars stars4">2stars</label>
                                                                    <span class="count">75</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="rating" value="3stars" id="3stars" class="radio-btn">
                                                                    <label for="3stars" class="radio-label stars stars3">3stars</label>
                                                                    <span class="count">35</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="rating" value="4stars" id="4stars" class="radio-btn">
                                                                    <label for="4stars" class="radio-label stars stars2">4stars</label>
                                                                    <span class="count">34</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="rating" value="5stars" id="5stars" class="radio-btn">
                                                                    <label for="5stars" class="radio-label stars stars1">5stars</label>
                                                                    <span class="count">65</span>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-1">
                                                    <div class="accommodation-widget widget">
                                                        <div class="title-widget">
                                                            <div class="title">accommodation</div>
                                                        </div>
                                                        <div class="content-widget">
                                                            <form class="radio-selection">
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="accommodation" value="Hotel" id="Hotel" class="radio-btn">
                                                                    <label for="Hotel" class="radio-label">Hotel</label>
                                                                    <span class="count">27</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="accommodation" value="Hostel" id="Hostel" class="radio-btn">
                                                                    <label for="Hostel" class="radio-label">Hostel</label>
                                                                    <span class="count">75</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="accommodation" value="Resort" id="Resort" class="radio-btn">
                                                                    <label for="Resort" class="radio-label">Resort</label>
                                                                    <span class="count">35</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="accommodation" value="Villa" id="Villa" class="radio-btn">
                                                                    <label for="Villa" class="radio-label">Villa</label>
                                                                    <span class="count">34</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="accommodation" value="Motel" id="Motel" class="radio-btn">
                                                                    <label for="Motel" class="radio-label">Motel</label>
                                                                    <span class="count">65</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="accommodation" value="Bungalow" id="Bungalow" class="radio-btn">
                                                                    <label for="Bungalow" class="radio-label">Bungalow</label>
                                                                    <span class="count">65</span>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="col-1">
                                                    <div class="stop-widget widget">
                                                        <div class="title-widget">
                                                            <div class="title">facilities</div>
                                                        </div>
                                                        <div class="content-widget">
                                                            <form class="radio-selection">
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="facilities" value="Swimming-pool" id="Swimming-pool" class="radio-btn">
                                                                    <label for="Swimming-pool" class="radio-label">Swimming Pool</label>
                                                                    <span class="count">27</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="facilities" value="Restaurant" id="Restaurant" class="radio-btn">
                                                                    <label for="Restaurant" class="radio-label">Restaurant</label>
                                                                    <span class="count">75</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="facilities" value="Airport" id="Airport" class="radio-btn">
                                                                    <label for="Airport" class="radio-label">Airport Transfer</label>
                                                                    <span class="count">35</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="facilities" value="Nightclub" id="Nightclub" class="radio-btn">
                                                                    <label for="Nightclub" class="radio-label">Nightclub</label>
                                                                    <span class="count">34</span>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- BUTTON BACK TO TOP-->
                <div id="back-top">
                    <a href="#top" class="link">
                        <i class="fa fa-angle-double-up"></i>
                    </a>
                </div>
            </div>
            <!-- FOOTER-->

                <?php include('footer.php');?>

        </div>
        </div>
        <div class="theme-setting">
            <div class="theme-loading">
                <div class="theme-loading-content">
                    <div class="dots-loader"></div>
                </div>
            </div>
            <a href="javascript:;" class="btn-theme-setting">
                <i class="fa fa-tint"></i>
            </a>
            
        </div>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('.logo .header-logo img ,.logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-' + Cookies.get('color-skin') + '.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-' + Cookies.get('color-skin') + '.png');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('.logo .header-logo img , .logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-color-1.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-color-1.png');
            }
        </script>
        <!-- LIBRARY JS-->
        <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/libs/detect-browser/browser.js"></script>
        <script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
        <script src="assets/libs/wow-js/wow.min.js"></script>
        <script src="assets/libs/slick-slider/slick.min.js"></script>
        <script src="assets/libs/selectbox/js/jquery.selectbox-0.2.js"></script>
        <script src="assets/libs/please-wait/please-wait.min.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-thumbs.js"></script>
        <!--script(src="assets/libs/parallax/jquery.data-parallax.min.js")-->
        <!-- MAIN JS-->
        <script src="assets/js/main.js"></script>
        <!-- LOADING JS FOR PAGE-->
        <script src="assets/js/pages/result.js"></script>
        <script src="assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script>
            var logo_str = 'assets/images/logo/logo-black-color-1.png';
            if (Cookies.set('color-skin'))
            {
                logo_str = 'assets/images/logo/logo-black-' + Cookies.set('color-skin') + '.png';
            }
            window.loading_screen = window.pleaseWait(
            {
                logo: logo_str,
                backgroundColor: '#fff',
                loadingHtml: "<div class='spinner sk-spinner-wave'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div>",
            });
        </script>
    </body>
</html>