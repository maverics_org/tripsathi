<div class="container">
    <ul class="topbar-left list-unstyled list-inline pull-left">
    <li>
        <a href="javascript:void(0)" class="country dropdown-text">
            <span>Country</span>
                <i class="topbar-icon icons-dropdown fa fa-angle-down"></i>
        </a>
        <ul class="dropdown-topbar list-unstyled hide">
            <li><a href="#" class="link">India</a></li>
            <li><a href="#" class="link">Dubai</a></li>
            <li><a href="#" class="link">Singapore</a></li>
        </ul>
    </li>
    <li>
        <a href="javascript:void(0)" class="language dropdown-text">
            <span>English</span>
            <i class="topbar-icon icons-dropdown fa fa-angle-down"></i>
        </a>
        <ul class="dropdown-topbar list-unstyled hide">
            <li><a href="#" class="link">India</a></li>
            <li><a href="#" class="link">Dubai</a></li>
            <li><a href="#" class="link">Singapore</a></li>
        </ul>
    </li>
    <li>
        <a href="javascript:void(0)" class="monney dropdown-text">
            <span>USD</span>
            <i class="topbar-icon icons-dropdown fa fa-angle-down"></i>
        </a>
        <ul class="dropdown-topbar list-unstyled hide">
            <li><a href="#" class="link">INR</a></li>
            <li><a href="#" class="link">USD</a></li>
        </ul>
    </li>
    </ul>
    <ul class="topbar-right pull-right list-unstyled list-inline login-widget">
        <li><a href="sign-in.php" class="item">login</a></li>
        <li><a href="register.php" class="item">register</a></li>
    </ul>
</div>