<!DOCTYPE html>
<html lang="en">
<head>
        <title>Tripsathi | Contact</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome/css/font-awesome.css">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-flaticon/flaticon.css">
        <!-- LIBRARY CSS-->
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/animate/animate.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick-theme.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/selectbox/css/jquery.selectbox.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/please-wait/please-wait.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox8cbb.css?v=2.1.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-buttons3447.css?v=1.0.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-thumbsf2ad.css?v=1.0.7">
        <!-- STYLE CSS-->
        <link type="text/css" rel="stylesheet" href="assets/css/layout.css">
        <link type="text/css" rel="stylesheet" href="assets/css/components.css">
        <link type="text/css" rel="stylesheet" href="assets/css/responsive.css">
        <link type="text/css" rel="stylesheet" href="assets/css/color.css">
        <!--link(type="text/css", rel='stylesheet', href='assets/css/color-1/color-1.css', id="color-skins")-->
        <link type="text/css" rel="stylesheet" href="#" id="color-skins">
        <script src="assets/libs/jquery/jquery-2.2.3.min.js"></script>
        <script src="assets/libs/js-cookie/js.cookie.js"></script>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/' + Cookies.get('color-skin') + '/' + 'color.css');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/color-1/color.css');
            }
        </script>

    </head>
    <body>
        <div class="body-wrapper">
            <!-- MENU MOBILE-->
            
            <?php include('mobile-menu.php');?>
            <!-- WRAPPER CONTENT-->
            <div class="wrapper-content">
                <!-- HEADER-->
                
                <?php include('header-2.php');?>
                <!-- WRAPPER-->
                <div id="wrapper-content">
                    <!-- MAIN CONTENT-->
                    <div class="main-content">
                        <section class="page-banner contact-page">
                            <div class="container">
                                <div class="page-title-wrapper">
                                    <div class="page-title-content">
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="index.php" class="link home">Home</a>
                                            </li>
                                            <li class="active">
                                                <a href="#" class="link">contact</a>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <h2 class="captions">contact</h2>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="padding-top padding-bottom contact-organization">
                            <div class="container">
                                <h3 class="title-style-2">Our organization</h3>
                                <div class="row">
                                    <div class="wrapper-organization">
                                        <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                                            <div class="content-organization">
                                                <div class="wrapper-img">
                                                    <img src="assets/images/homepage/avatar-contact-1.jpg" alt="" class="img img-responsive">
                                                </div>
                                                <div class="main-organization">
                                                    <div class="organization-title">
                                                        <a href="#" class="title">Jason Williams</a>
                                                        <p class="text">Commercial Director</p>
                                                    </div>
                                                    <div class="content-widget">
                                                        <div class="info-list">
                                                            <ul class="list-unstyled">
                                                                <li class="main-list">
                                                                    <i class="icons fa fa-map-marker"></i>
                                                                    <a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a>
                                                                </li>
                                                                <li class="main-list">
                                                                    <i class="icons fa fa-phone"></i>
                                                                    <a href="#" class="link">910-740-6026</a>
                                                                </li>
                                                                <li class="main-list">
                                                                    <i class="icons fa fa-envelope-o"></i>
                                                                    <a href="#" class="link">domain@expooler.com</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                                            <div class="content-organization">
                                                <div class="wrapper-img">
                                                    <img src="assets/images/homepage/avatar-contact-2.jpg" alt="" class="img img-responsive">
                                                </div>
                                                <div class="main-organization">
                                                    <div class="organization-title">
                                                        <a href="#" class="title">Jason Williams</a>
                                                        <p class="text">Commercial Director</p>
                                                    </div>
                                                    <div class="content-widget">
                                                        <div class="info-list">
                                                            <ul class="list-unstyled">
                                                                <li class="main-list">
                                                                    <i class="icons fa fa-map-marker"></i>
                                                                    <a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a>
                                                                </li>
                                                                <li class="main-list">
                                                                    <i class="icons fa fa-phone"></i>
                                                                    <a href="#" class="link">910-740-6026</a>
                                                                </li>
                                                                <li class="main-list">
                                                                    <i class="icons fa fa-envelope-o"></i>
                                                                    <a href="#" class="link">domain@expooler.com</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                                            <div class="content-organization">
                                                <div class="wrapper-img">
                                                    <img src="assets/images/homepage/avatar-contact-3.jpg" alt="" class="img img-responsive">
                                                </div>
                                                <div class="main-organization">
                                                    <div class="organization-title">
                                                        <a href="#" class="title">Jason Williams</a>
                                                        <p class="text">Commercial Director</p>
                                                    </div>
                                                    <div class="content-widget">
                                                        <div class="info-list">
                                                            <ul class="list-unstyled">
                                                                <li class="main-list">
                                                                    <i class="icons fa fa-map-marker"></i>
                                                                    <a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a>
                                                                </li>
                                                                <li class="main-list">
                                                                    <i class="icons fa fa-phone"></i>
                                                                    <a href="#" class="link">910-740-6026</a>
                                                                </li>
                                                                <li class="main-list">
                                                                    <i class="icons fa fa-envelope-o"></i>
                                                                    <a href="#" class="link">domain@expooler.com</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="contact style-1 page-contact-form padding-top padding-bottom">
                            <div class="container">
                                <div class="wrapper-contact-form">
                                    <div data-wow-delay="0.5s" class="contact-wrapper wow fadeInLeft">
                                        <div class="contact-box">
                                            <h5 class="title">CONTACT FORM</h5>
                                            <p class="text">Subcribe to receive new properties with good price.</p>
                                            
<form method="POST" name="contact" action="contact">
                                                <input type="text" placeholder="Your Name" class="form-control form-input">
                                                <!--label.control-label.form-label.warning-label(for="") Warning for the above !-->
                                                <input type="email" placeholder="Your Email" class="form-control form-input">
                                                <!--label.control-label.form-label.warning-label(for="") Warning for the above !-->
                                                <textarea placeholder="message" class="form-control form-input"></textarea>
                                                <div class="contact-submit">
                                                    <button type="submit" data-hover="SEND NOW" class="btn btn-slide">
                                                        <span class="text">send message</span>
                                                        <span class="icons fa fa-long-arrow-right"></span>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div data-wow-delay="0.5s" class="wrapper-form-images wow fadeInRight">
                                        <img src="assets/images/background/bg-banner-contact-form.jpg" alt="" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="page-contact-map">
                            <div class="map-block">
                                <div class="wrapper-info">
                                    <div class="map-info">
                                        <h3 class="title-style-2">HOW TO FIND US</h3>
                                        <p class="address">
                                            <i class="fa fa-map-marker"></i> 333 Moo 10, Chert Wudthakas Road, Srikan, Don Mueang, Bangkok, Thailand</p>
                                        <p class="phone">
                                            <i class="fa fa-phone"></i> 910-740-6026</p>
                                        <p class="mail">
                                            <a href="mailto:domain@expooler.com">
                                                <i class="fa fa-envelope-o"></i>domain@expooler.com</a>
                                        </p>
                                        <div class="footer-block">
                                            <a class="btn btn-open-map">Open Map</a>
                                        </div>
                                    </div>
                                </div>
                                <div id="googleMap"></div>
                            </div>
                        </section>
                    </div>
                    <!-- BUTTON BACK TO TOP-->
                    <div id="back-top">
                        <a href="#top" class="link">
                            <i class="fa fa-angle-double-up"></i>
                        </a>
                    </div>
                </div>
                <!-- FOOTER-->
                <?php include('footer.php');?>
            </div>
        </div>
        <div class="theme-setting">
            <div class="theme-loading">
                <div class="theme-loading-content">
                    <div class="dots-loader"></div>
                </div>
            </div>
            <a href="javascript:;" class="btn-theme-setting">
                <i class="fa fa-tint"></i>
            </a>
            
        </div>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('.logo .header-logo img ,.logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-' + Cookies.get('color-skin') + '.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-' + Cookies.get('color-skin') + '.png');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('.logo .header-logo img , .logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-color-1.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-color-1.png');
            }
        </script>
        <!-- LIBRARY JS-->
        <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/libs/detect-browser/browser.js"></script>
        <script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
        <script src="assets/libs/wow-js/wow.min.js"></script>
        <script src="assets/libs/slick-slider/slick.min.js"></script>
        <script src="assets/libs/selectbox/js/jquery.selectbox-0.2.js"></script>
        <script src="assets/libs/please-wait/please-wait.min.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-thumbs.js"></script>
        <!--script(src="assets/libs/parallax/jquery.data-parallax.min.js")-->
        <!-- MAIN JS-->
        <script src="assets/js/main.js"></script>
        <!-- LOADING JS FOR PAGE-->
        <script src="assets/js/pages/contact.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js"></script>
        <script>
            var logo_str = 'assets/images/logo/logo-black-color-1.png';
            if (Cookies.set('color-skin'))
            {
                logo_str = 'assets/images/logo/logo-black-' + Cookies.set('color-skin') + '.png';
            }
            window.loading_screen = window.pleaseWait(
            {
                logo: logo_str,
                backgroundColor: '#fff',
                loadingHtml: "<div class='spinner sk-spinner-wave'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div>",
            });
        </script>
    </body>
</html>