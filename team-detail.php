<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tripsathi | team-detail</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome/css/font-awesome.css">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-flaticon/flaticon.css">
        <!-- LIBRARY CSS-->
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/animate/animate.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick-theme.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/selectbox/css/jquery.selectbox.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/please-wait/please-wait.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox8cbb.css?v=2.1.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-buttons3447.css?v=1.0.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-thumbsf2ad.css?v=1.0.7">
        <!-- STYLE CSS-->
        <link type="text/css" rel="stylesheet" href="assets/css/layout.css">
        <link type="text/css" rel="stylesheet" href="assets/css/components.css">
        <link type="text/css" rel="stylesheet" href="assets/css/responsive.css">
        <link type="text/css" rel="stylesheet" href="assets/css/color.css">
        <!--link(type="text/css", rel='stylesheet', href='assets/css/color-1/color-1.css', id="color-skins")-->
        <link type="text/css" rel="stylesheet" href="#" id="color-skins">
        <script src="assets/libs/jquery/jquery-2.2.3.min.js"></script>
        <script src="assets/libs/js-cookie/js.cookie.js"></script>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/' + Cookies.get('color-skin') + '/' + 'color.css');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/color-1/color.css');
            }
        </script>
    </head>
    <body>
        <div class="body-wrapper">
            <!-- MENU MOBILE-->
            
            <?php include('mobile-menu.php');?>
            <!-- WRAPPER CONTENT-->
            <div class="wrapper-content">
                <!-- HEADER-->
                
                <?php include('header-2.php');?>
                <!-- WRAPPER-->
                <div id="wrapper-content">
                    <!-- MAIN CONTENT-->
                    <div class="main-content">
                        <section class="page-banner bg-team">
                            <div class="container">
                                <div class="page-title-wrapper">
                                    <div class="page-title-content">
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="index.html" class="link home">Home</a>
                                            </li>
                                            <li class="active">
                                                <a href="#" class="link">team</a>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <h2 class="captions">team</h2>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="page-main">
                            <div class="section-faq padding-top padding-bottom">
                                <div class="container">
                                    <div class="wrapper-team-detail">
                                        <h3 class="title-style-2">team detail</h3>
                                        <div class="main-team padding-bottom">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-5 padding-col-right">
                                                    <div class="content-team-detail">
                                                        <div class="content-expert">
                                                            <a href="#" class="img-expert">
                                                                <img src="assets/images/homepage/team-2.jpg" alt="" class="img-responsive img">
                                                            </a>
                                                            <div class="caption-expert">
                                                                <div class="item-expert">
                                                                    <i class="icon-expert fa fa-comment-o"></i>
                                                                    <a href="#" class="title">Talk to me!</a>
                                                                </div>
                                                                <ul class="social list-inline">
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-facebook"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-twitter"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-pinterest-p"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-google"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="content-expert">
                                                            <a href="#" class="img-expert">
                                                                <img src="assets/images/homepage/team-5.jpg" alt="" class="img-responsive img">
                                                            </a>
                                                            <div class="caption-expert">
                                                                <div class="item-expert">
                                                                    <i class="icon-expert fa fa-comment-o"></i>
                                                                    <a href="#" class="title">Talk to me!</a>
                                                                </div>
                                                                <ul class="social list-inline">
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-facebook"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-twitter"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-pinterest-p"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-google"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="content-expert">
                                                            <a href="#" class="img-expert">
                                                                <img src="assets/images/homepage/team-3.jpg" alt="" class="img-responsive img">
                                                            </a>
                                                            <div class="caption-expert">
                                                                <div class="item-expert">
                                                                    <i class="icon-expert fa fa-comment-o"></i>
                                                                    <a href="#" class="title">Talk to me!</a>
                                                                </div>
                                                                <ul class="social list-inline">
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-facebook"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-twitter"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-pinterest-p"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-google"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="content-expert">
                                                            <a href="#" class="img-expert">
                                                                <img src="assets/images/homepage/team-4.jpg" alt="" class="img-responsive img">
                                                            </a>
                                                            <div class="caption-expert">
                                                                <div class="item-expert">
                                                                    <i class="icon-expert fa fa-comment-o"></i>
                                                                    <a href="#" class="title">Talk to me</a>
                                                                </div>
                                                                <ul class="social list-inline">
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-facebook"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-twitter"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-pinterest-p"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-google"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="content-expert">
                                                            <a href="#" class="img-expert">
                                                                <img src="assets/images/homepage/team-1.jpg" alt="" class="img-responsive img">
                                                            </a>
                                                            <div class="caption-expert">
                                                                <div class="item-expert">
                                                                    <i class="icon-expert fa fa-comment-o"></i>
                                                                    <a href="#" class="title">Talk to me!</a>
                                                                </div>
                                                                <ul class="social list-inline">
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-facebook"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-twitter"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-pinterest-p"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="social-expert">
                                                                            <i class="expert-icon fa fa-google"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-7 padding-col-left">
                                                    <div class="wrapper-caption-team">
                                                        <div class="wrapper-team-title">
                                                            <a href="#" class="team-title">mark letto</a>
                                                            <p class="team-title-small">Manager Tour Guide</p>
                                                            <div class="team-title-andress">
                                                                <i class="team-icon fa fa-map-marker"></i>
                                                                <a href="#" class="item-andress">New York City, United States</a>
                                                            </div>
                                                        </div>
                                                        <p class="text">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                                            duis aute.</p>
                                                        <p class="text">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                                            duis aute irure dolor. Consectetur adiiing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                                        <div class="group-list">
                                                            <ul class="list-unstyled about-us-list">
                                                                <li>
                                                                    <p class="text">See around 100 shows per year</p>
                                                                </li>
                                                                <li>
                                                                    <p class="text">Love sharing my knowledge with people from around the world</p>
                                                                </li>
                                                                <li>
                                                                    <p class="text">Have performed Off-Broadway with an Oscar nominee</p>
                                                                </li>
                                                                <li>
                                                                    <p class="text">Know the theatre business from every angle </p>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="title-style-2">teammate</h3>
                                        <div class="wrapper-expert team-profile">
                                            <div class="content-expert">
                                                <a class="img-expert">
                                                    <img src="assets/images/homepage/about-6.jpg" alt="" class="img-responsive img">
                                                </a>
                                                <div class="caption-expert">
                                                    <div class="item-expert">
                                                        <i class="icon-expert fa fa-comment-o"></i>
                                                        <a href="#" class="title">Talk to me!</a>
                                                    </div>
                                                    <ul class="social list-inline">
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-pinterest-p"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-google"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="content-expert">
                                                <a class="img-expert">
                                                    <img src="assets/images/homepage/about-9.jpg" alt="" class="img-responsive img">
                                                </a>
                                                <div class="caption-expert">
                                                    <div class="item-expert">
                                                        <i class="icon-expert fa fa-comment-o"></i>
                                                        <a href="#" class="title">Talk to me!</a>
                                                    </div>
                                                    <ul class="social list-inline">
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-pinterest-p"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-google"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="content-expert">
                                                <a class="img-expert">
                                                    <img src="assets/images/homepage/about-7.jpg" alt="" class="img-responsive img">
                                                </a>
                                                <div class="caption-expert">
                                                    <div class="item-expert">
                                                        <i class="icon-expert fa fa-comment-o"></i>
                                                        <a href="#" class="title">Talk to me!</a>
                                                    </div>
                                                    <ul class="social list-inline">
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-pinterest-p"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-google"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="content-expert">
                                                <a class="img-expert">
                                                    <img src="assets/images/homepage/about-8.jpg" alt="" class="img-responsive img">
                                                </a>
                                                <div class="caption-expert">
                                                    <div class="item-expert">
                                                        <i class="icon-expert fa fa-comment-o"></i>
                                                        <a href="#" class="title">Talk to me!</a>
                                                    </div>
                                                    <ul class="social list-inline">
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-pinterest-p"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-google"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="content-expert">
                                                <a class="img-expert">
                                                    <img src="assets/images/homepage/about-5.jpg" alt="" class="img-responsive img">
                                                </a>
                                                <div class="caption-expert">
                                                    <div class="item-expert">
                                                        <i class="icon-expert fa fa-comment-o"></i>
                                                        <a href="#" class="title">Talk to me!</a>
                                                    </div>
                                                    <ul class="social list-inline">
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-pinterest-p"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="social-expert">
                                                                <i class="expert-icon fa fa-google"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <section class="about-tours">
                                <div class="container">
                                    <div class="team-purchase">
                                        <div class="purchase-title">
                                            <h2 class="main-title">Be More Than Just Another Traveler When You
                                                <span class="title-yellow">Explooer</span>
                                            </h2>
                                        </div>
                                        <p class="text">Discover your next great adventure, become an explorer to get started!</p>
                                        <a href="#" class="btn btn-maincolor">Purchase now</a>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <!-- BUTTON BACK TO TOP-->
                    <div id="back-top">
                        <a href="#top" class="link">
                            <i class="fa fa-angle-double-up"></i>
                        </a>
                    </div>
                </div>
                <!-- FOOTER-->

                <?php include('footer.php');?>

            </div>
        </div>
        <div class="theme-setting">
            <div class="theme-loading">
                <div class="theme-loading-content">
                    <div class="dots-loader"></div>
                </div>
            </div>
            <a href="javascript:;" class="btn-theme-setting">
                <i class="fa fa-tint"></i>
            </a>
            
        </div>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('.logo .header-logo img ,.logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-' + Cookies.get('color-skin') + '.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-' + Cookies.get('color-skin') + '.png');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('.logo .header-logo img , .logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-color-1.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-color-1.png');
            }
        </script>
        <!-- LIBRARY JS-->
        <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/libs/detect-browser/browser.js"></script>
        <script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
        <script src="assets/libs/wow-js/wow.min.js"></script>
        <script src="assets/libs/slick-slider/slick.min.js"></script>
        <script src="assets/libs/selectbox/js/jquery.selectbox-0.2.js"></script>
        <script src="assets/libs/please-wait/please-wait.min.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-thumbs.js"></script>
        <!--script(src="assets/libs/parallax/jquery.data-parallax.min.js")-->
        <!-- MAIN JS-->
        <script src="assets/js/main.js"></script>
        <!-- LOADING JS FOR PAGE-->
        <script src="assets/js/pages/team.js"></script>
        <script>
            var logo_str = 'assets/images/logo/logo-black-color-1.png';
            if (Cookies.set('color-skin'))
            {
                logo_str = 'assets/images/logo/logo-black-' + Cookies.set('color-skin') + '.png';
            }
            window.loading_screen = window.pleaseWait(
            {
                logo: logo_str,
                backgroundColor: '#fff',
                loadingHtml: "<div class='spinner sk-spinner-wave'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div>",
            });
        </script>
    </body>
</html>