<!DOCTYPE html>
<html lang="en">
<head>
        <title>Tripsathi | Blog</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome/css/font-awesome.css">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-flaticon/flaticon.css">
        <!-- LIBRARY CSS-->
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/animate/animate.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick-theme.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/selectbox/css/jquery.selectbox.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/please-wait/please-wait.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox8cbb.css?v=2.1.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-buttons3447.css?v=1.0.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-thumbsf2ad.css?v=1.0.7">
        <!-- STYLE CSS-->
        <link type="text/css" rel="stylesheet" href="assets/css/layout.css">
        <link type="text/css" rel="stylesheet" href="assets/css/components.css">
        <link type="text/css" rel="stylesheet" href="assets/css/responsive.css">
        <link type="text/css" rel="stylesheet" href="assets/css/color.css">
        <!--link(type="text/css", rel='stylesheet', href='assets/css/color-1/color-1.css', id="color-skins")-->
        <link type="text/css" rel="stylesheet" href="#" id="color-skins">
        <script src="assets/libs/jquery/jquery-2.2.3.min.js"></script>
        <script src="assets/libs/js-cookie/js.cookie.js"></script>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/' + Cookies.get('color-skin') + '/' + 'color.css');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/color-1/color.css');
            }
        </script>
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
    </head>
    <body>
        <div class="body-wrapper">
            <!-- MENU MOBILE-->
            
            <?php include('mobile-menu.php');?>
                        <!-- WRAPPER CONTENT-->
            <div class="wrapper-content">
                <!-- HEADER-->
                
                <?php include('header-2.php');?>
                <!-- WRAPPER-->
                <div id="wrapper-content">
                    <!-- MAIN CONTENT-->
                    <div class="main-content">
                        <section class="page-banner blog">
                            <div class="container">
                                <div class="page-title-wrapper">
                                    <div class="page-title-content">
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="index.html" class="link home">Home</a>
                                            </li>
                                            <li class="active">
                                                <a href="#" class="link">BLOG</a>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <h2 class="captions">BLOG</h2>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="page-main padding-top padding-bottom">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 main-left blog-wrapper">
                                        <div class="blog-post">
                                            <div class="blog-image">
                                                <a href="javascript:void(0)" class="link">
                                                    <img src="assets/images/blog/blog-image-1.jpg" alt="a car on a road" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="blog-content">
                                                <div class="col-xs-2">
                                                    <div class="row">
                                                        <div class="date">
                                                            <h1 class="day">07</h1>
                                                            <div class="month">JAN</div>
                                                            <div class="year">2016</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-10 content-wrapper">
                                                    <a href="javascript:void(0)" class="heading">Many people limit themselves what they think they can do.</a>
                                                    <h5 class="meta-info">Posted By :
                                                        <span>John Smith</span>
                                                        <span class="sep">/</span>
                                                        <span class="view-count fa-custom">56</span>
                                                        <span class="comment-count fa-custom">239</span>
                                                    </h5>
                                                    <p class="preview">Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra augue.
                                                        Donec ullamcorper nulla non metus auctor fringilla. Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus , tortor mauris condimentum nibh, ut...</p>
                                                    <a href="javascript:void(0)"
                                                        class="btn btn-gray btn-fit btn-capitalize">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="blog-post">
                                            <div class="blog-video">
                                                <div class="video-thumbnail">
                                                    <div class="video-bg">
                                                        <img src="assets/images/blog/blog-video-poster-1.jpg" alt="A traveler standing on a rock" class="img-responsive">
                                                    </div>
                                                    <div class="video-button-play">
                                                        <i class="icons fa fa-play"></i>
                                                    </div>
                                                    <div class="video-button-close"></div>
                                                    <iframe src="https://www.youtube.com/embed/moOosWuoDyA?rel=0" allowfullscreen="allowfullscreen" class="video-embed"></iframe>
                                                </div>
                                            </div>
                                            <div class="blog-content">
                                                <div class="col-xs-2">
                                                    <div class="row">
                                                        <div class="date">
                                                            <h1 class="day">07</h1>
                                                            <div class="month">JAN</div>
                                                            <div class="year">2016</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-10 content-wrapper">
                                                    <a href="javascript:void(0)" class="heading">Many people limit themselves what they think they can do.</a>
                                                    <h5 class="meta-info">Posted By :
                                                        <span>John Smith</span>
                                                        <span class="sep">/</span>
                                                        <span class="view-count fa-custom">56</span>
                                                        <span class="comment-count fa-custom">239</span>
                                                    </h5>
                                                    <p class="preview">Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra augue.
                                                        Donec ullamcorper nulla non metus auctor fringilla. Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus , tortor mauris condimentum nibh, ut...</p>
                                                    <a href="javascript:void(0)"
                                                        class="btn btn-gray btn-fit btn-capitalize">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="blog-post">
                                            <div class="blog-content">
                                                <div class="col-xs-2">
                                                    <div class="row">
                                                        <div class="date">
                                                            <h1 class="day">06</h1>
                                                            <div class="month">DEC</div>
                                                            <div class="year">2016</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-10 quote-content-wrapper content-wrapper">
                                                    <blockquote class="quote-wrapper fa-custom">
                                                        <a href="javascript:void(0)" class="quote">Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis a.</a>
                                                        <footer>
                                                            <span class="author">Amber Ivy</span>
                                                            <span class="company">(Grancie Company)</span>
                                                        </footer>
                                                    </blockquote>
                                                    <p class="preview">Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra augue.
                                                        Donec er nulla non metus auctor fringilla est non commodo luctus.</p>
                                                    <a href="javascript:void(0)" class="btn btn-gray btn-fit btn-capitalize">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="blog-post">
                                            <div class="blog-image">
                                                <a href="javascript:void(0)" class="link">
                                                    <img src="assets/images/blog/blog-image-1.jpg" alt="a car on a road" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="blog-content">
                                                <div class="col-xs-2">
                                                    <div class="row">
                                                        <div class="date">
                                                            <h1 class="day">07</h1>
                                                            <div class="month">JAN</div>
                                                            <div class="year">2016</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-10 content-wrapper">
                                                    <a href="javascript:void(0)" class="heading">Many people limit themselves what they think they can do.</a>
                                                    <h5 class="meta-info">Posted By :
                                                        <span>John Smith</span>
                                                        <span class="sep">/</span>
                                                        <span class="view-count fa-custom">56</span>
                                                        <span class="comment-count fa-custom">239</span>
                                                    </h5>
                                                    <p class="preview">Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra augue.
                                                        Donec ullamcorper nulla non metus auctor fringilla. Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus , tortor mauris condimentum nibh, ut...</p>
                                                    <a href="javascript:void(0)"
                                                        class="btn btn-gray btn-fit btn-capitalize">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                        <nav class="pagination-list margin-top70">
                                            <ul class="pagination">
                                                <li>
                                                    <a href="#" aria-label="Previous" class="btn-pagination previous">
                                                        <span aria-hidden="true" class="fa fa-angle-left"></span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-pagination active">01</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-pagination">02</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-pagination">03</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn-pagination">...</a>
                                                </li>
                                                <li>
                                                    <a href="#" aria-label="Next" class="btn-pagination next">
                                                        <span aria-hidden="true" class="fa fa-angle-right"></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="col-md-4 sidebar-widget">
                                        <div class="col-2">
                                            <div class="search-widget widget">
                                                <form>
                                                    <div class="input-group search-wrapper">
                                                        <input type="text" placeholder="Search..." class="search-input form-control">
                                                        <span class="input-group-btn">
                                                            <button type="submit" class="btn submit-btn">
                                                                <span class="fa fa-search"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="col-1">
                                                <div class="recent-post-widget widget">
                                                    <div class="title-widget">
                                                        <div class="title">RECENT POST</div>
                                                    </div>
                                                    <div class="content-widget">
                                                        <div class="recent-post-list">
                                                            <div class="single-widget-item">
                                                                <div class="single-recent-post-widget">
                                                                    <a href="javascript:void(0)" class="thumb img-wrapper">
                                                                        <img src="assets/images/blog/recent-blog-post/post-1.jpg" alt="recent post picture 1">
                                                                    </a>
                                                                    <div class="post-info">
                                                                        <div class="meta-info">
                                                                            <span>Aug 18, 2016</span>
                                                                            <span class="sep">/</span>
                                                                            <span class="fa-custom view-count">56</span>
                                                                            <span class="fa-custom comment-count">239</span>
                                                                        </div>
                                                                        <div class="single-rp-preview">Donec ullamcorper nulla non metus nisi auctor fringilla they can do.</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-widget-item">
                                                                <div class="single-recent-post-widget">
                                                                    <a href="javascript:void(0)" class="thumb img-wrapper">
                                                                        <img src="assets/images/blog/recent-blog-post/post-2.jpg" alt="recent post picture 2">
                                                                    </a>
                                                                    <div class="post-info">
                                                                        <div class="meta-info">
                                                                            <span>Aug 18, 2016</span>
                                                                            <span class="sep">/</span>
                                                                            <span class="fa-custom view-count">56</span>
                                                                            <span class="fa-custom comment-count">239</span>
                                                                        </div>
                                                                        <div class="single-rp-preview">Donec ullamcorper nulla non metus nisi auctor fringilla they can do.</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-widget-item">
                                                                <div class="single-recent-post-widget">
                                                                    <a href="javascript:void(0)" class="thumb img-wrapper">
                                                                        <img src="assets/images/blog/recent-blog-post/post-3.jpg" alt="recent post picture 3">
                                                                    </a>
                                                                    <div class="post-info">
                                                                        <div class="meta-info">
                                                                            <span>Aug 18, 2016</span>
                                                                            <span class="sep">/</span>
                                                                            <span class="fa-custom view-count">56</span>
                                                                            <span class="fa-custom comment-count">239</span>
                                                                        </div>
                                                                        <div class="single-rp-preview">Donec ullamcorper nulla non metus nisi auctor fringilla they can do.</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-widget-item">
                                                                <div class="single-recent-post-widget">
                                                                    <a href="javascript:void(0)" class="thumb img-wrapper">
                                                                        <img src="assets/images/blog/recent-blog-post/post-4.jpg" alt="recent post picture 4">
                                                                    </a>
                                                                    <div class="post-info">
                                                                        <div class="meta-info">
                                                                            <span>Aug 18, 2016</span>
                                                                            <span class="sep">/</span>
                                                                            <span class="fa-custom view-count">56</span>
                                                                            <span class="fa-custom comment-count">239</span>
                                                                        </div>
                                                                        <div class="single-rp-preview">Donec ullamcorper nulla non metus nisi auctor fringilla they can do.</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-widget-item">
                                                                <div class="single-recent-post-widget">
                                                                    <a href="javascript:void(0)" class="thumb img-wrapper">
                                                                        <img src="assets/images/blog/recent-blog-post/post-5.jpg" alt="recent post picture 5">
                                                                    </a>
                                                                    <div class="post-info">
                                                                        <div class="meta-info">
                                                                            <span>Aug 18, 2016</span>
                                                                            <span class="sep">/</span>
                                                                            <span class="fa-custom view-count">56</span>
                                                                            <span class="fa-custom comment-count">239</span>
                                                                        </div>
                                                                        <div class="single-rp-preview">Donec ullamcorper nulla non metus nisi auctor fringilla they can do.</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1">
                                                <div class="categories-widget widget">
                                                    <div class="title-widget">
                                                        <div class="title">CATEGORIES</div>
                                                    </div>
                                                    <div class="content-widget">
                                                        <ul class="widget-list">
                                                            <li class="single-widget-item">
                                                                <a href="javascript:void(0)" class="link">
                                                                    <span class="fa-custom category">Travel</span>
                                                                    <span class="count">27</span>
                                                                </a>
                                                            </li>
                                                            <li class="single-widget-item">
                                                                <a href="javascript:void(0)" class="link">
                                                                    <span class="fa-custom category">Explore</span>
                                                                    <span class="count">75</span>
                                                                </a>
                                                            </li>
                                                            <li class="single-widget-item">
                                                                <a href="javascript:void(0)" class="link">
                                                                    <span class="fa-custom category">Discover</span>
                                                                    <span class="count">35</span>
                                                                </a>
                                                            </li>
                                                            <li class="single-widget-item">
                                                                <a href="javascript:void(0)" class="link">
                                                                    <span class="fa-custom category">Adventure</span>
                                                                    <span class="count">34</span>
                                                                </a>
                                                            </li>
                                                            <li class="single-widget-item">
                                                                <a href="javascript:void(0)" class="link">
                                                                    <span class="fa-custom category">The world</span>
                                                                    <span class="count">65</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="tags-widget widget">
                                                    <div class="title-widget">
                                                        <div class="title">TAGS</div>
                                                    </div>
                                                    <div class="content-widget">
                                                        <a href="javascript:void(0)" class="tag">Explore</a>
                                                        <a href="javascript:void(0)" class="tag">Adventure</a>
                                                        <a href="javascript:void(0)" class="tag">Traveler</a>
                                                        <a href="javascript:void(0)" class="tag">Europe</a>
                                                        <a href="javascript:void(0)" class="tag">Explore</a>
                                                        <a href="javascript:void(0)" class="tag">Big Your World</a>
                                                        <a href="javascript:void(0)" class="tag">Adventure</a>
                                                        <a href="javascript:void(0)" class="tag">Adventure</a>
                                                        <a href="javascript:void(0)" class="tag">Traveler</a>
                                                        <a href="javascript:void(0)" class="tag">Explore</a>
                                                        <a href="javascript:void(0)" class="tag">Dream</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="col-1">
                                                <div class="archives-widget widget">
                                                    <div class="title-widget">
                                                        <div class="title">ARCHIVES</div>
                                                    </div>
                                                    <div class="content-widget">
                                                        <div class="archive-datepicker"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1">
                                                <div class="gallery-widget widget">
                                                    <div class="title-widget">
                                                        <div class="title">FROM GALLERY</div>
                                                    </div>
                                                    <div class="content-widget">
                                                        <ul class="list-unstyled list-inline">
                                                            <li>
                                                                <a href="javascript:void(0)" class="thumb">
                                                                    <img src="assets/images/gallery/gallery-widget-1.jpg" alt="gallery image 1" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="thumb">
                                                                    <img src="assets/images/gallery/gallery-widget-2.jpg" alt="gallery image 2" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="thumb">
                                                                    <img src="assets/images/gallery/gallery-widget-3.jpg" alt="gallery image 3" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="thumb">
                                                                    <img src="assets/images/gallery/gallery-widget-4.jpg" alt="gallery image 4" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="thumb">
                                                                    <img src="assets/images/gallery/gallery-widget-5.jpg" alt="gallery image 5" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="thumb">
                                                                    <img src="assets/images/gallery/gallery-widget-6.jpg" alt="gallery image 6" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="thumb">
                                                                    <img src="assets/images/gallery/gallery-widget-7.jpg" alt="gallery image 7" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="thumb">
                                                                    <img src="assets/images/gallery/gallery-widget-8.jpg" alt="gallery image 8" class="img-responsive">
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="social-widget widget">
                                                    <div class="title-widget">
                                                        <div class="title">SOCIAL</div>
                                                    </div>
                                                    <div class="content-widget">
                                                        <ul class="list-unstyled list-inline">
                                                            <li>
                                                                <a href="javascript:void(0)" class="social-icon fa fa-facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="social-icon fa fa-twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="social-icon fa fa-pinterest-p"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="social-icon fa fa-google"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="social-icon fa fa-rss"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="social-icon fa fa-facebook"></a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0)" class="social-icon fa fa-facebook"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- BUTTON BACK TO TOP-->
                    <div id="back-top">
                        <a href="#top" class="link">
                            <i class="fa fa-angle-double-up"></i>
                        </a>
                    </div>
                </div>
                <!-- FOOTER-->

                <?php include('footer.php');?>

            </div>
        </div>
        <div class="theme-setting">
            <div class="theme-loading">
                <div class="theme-loading-content">
                    <div class="dots-loader"></div>
                </div>
            </div>
            <a href="javascript:;" class="btn-theme-setting">
                <i class="fa fa-tint"></i>
            </a>
            
        </div>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('.logo .header-logo img ,.logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-' + Cookies.get('color-skin') + '.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-' + Cookies.get('color-skin') + '.png');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('.logo .header-logo img , .logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-color-1.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-color-1.png');
            }
        </script>
        <!-- LIBRARY JS-->
        <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/libs/detect-browser/browser.js"></script>
        <script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
        <script src="assets/libs/wow-js/wow.min.js"></script>
        <script src="assets/libs/slick-slider/slick.min.js"></script>
        <script src="assets/libs/selectbox/js/jquery.selectbox-0.2.js"></script>
        <script src="assets/libs/please-wait/please-wait.min.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-thumbs.js"></script>
        <!--script(src="assets/libs/parallax/jquery.data-parallax.min.js")-->
        <!-- MAIN JS-->
        <script src="assets/js/main.js"></script>
        <!-- LOADING JS FOR PAGE-->
        <script src="assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="assets/libs/nst-slider/js/jquery.nstSlider.min.js"></script>
        <script src="assets/libs/plus-minus-input/plus-minus-input.js"></script>
        <script src="assets/js/pages/sidebar.js"></script>
        <script src="assets/js/pages/blog.js"></script>
        <script>
            var logo_str = 'assets/images/logo/logo-black-color-1.png';
            if (Cookies.set('color-skin'))
            {
                logo_str = 'assets/images/logo/logo-black-' + Cookies.set('color-skin') + '.png';
            }
            window.loading_screen = window.pleaseWait(
            {
                logo: logo_str,
                backgroundColor: '#fff',
                loadingHtml: "<div class='spinner sk-spinner-wave'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div>",
            });
        </script>
    </body>
</html>