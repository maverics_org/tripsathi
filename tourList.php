<div class="tours-list">
    <div class="row">
        <?php 
        include "admin/database/config.php";
        $result_upp=mysql_query("select * from tour order by id desc");
        while($row_up=mysql_fetch_array($result_upp)) { ?>
        <div class="col-sm-6">
            <div class="tours-layout">
                <div class="image-wrapper">
                    <a href="<?php echo 'tour-view.php?id='.$row_up['id']; ?>" class="link">
                        <img src="assets/images/tours/tour-1.jpg" alt="" class="img-responsive">
                    </a>
                    <div class="title-wrapper">
                        <a href="#" class="title"><?php echo $row_up['tourToWhere']; ?></a>
                        <i class="icons flaticon-circle"></i>
                    </div>
                    <div class="label-sale">
                        <p class="text">sale</p>
                        <?php if($row_up['discount']) { ?><p class="number"><?php echo $row_up['discount']; ?>%</p>
                        <?php } ?>

                    </div>
                </div>
                <div class="content-wrapper">
                    <ul class="list-info list-inline list-unstyle">
                        <li>
                            <a href="#" class="link">
                                <i class="icons fa fa-eye"></i>
                                <span class="text number">234</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="link">
                                <i class="icons fa fa-heart"></i>
                                <span class="text number">234</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="link">
                                <i class="icons fa fa-comment"></i>
                                <span class="text number">234</span>
                            </a>
                        </li>
                    </ul>
                    <div class="content">
                        <div class="title">
                            <div class="price">
                                <sup>$</sup>
                                <span class="number">
                                    <?php echo $row_up['price']; ?>

                                </span>
                            </div>
                            <!-- <p class="for-price">3 <data></data>ys 2 nights</p> -->
                        </div>
                        <p class="text">
                            <?php echo $row_up['description']; ?>
                        </p>
                        <div class="group-btn-tours">
                            <a href="<?php echo 'tour-view.php?id='.$row_up['id']; ?>" class="left-btn">book now</a>
                            <a href="blog.html" class="right-btn">add to list</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>