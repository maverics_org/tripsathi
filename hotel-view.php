<!DOCTYPE html>
<html lang="en">
<head>
        <title>Tripsathi | Hotel View</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome/css/font-awesome.css">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-flaticon/flaticon.css">
        <!-- LIBRARY CSS-->
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/animate/animate.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick-theme.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/selectbox/css/jquery.selectbox.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/please-wait/please-wait.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox8cbb.css?v=2.1.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-buttons3447.css?v=1.0.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-thumbsf2ad.css?v=1.0.7">
        <!-- STYLE CSS-->
        <link type="text/css" rel="stylesheet" href="assets/css/layout.css">
        <link type="text/css" rel="stylesheet" href="assets/css/components.css">
        <link type="text/css" rel="stylesheet" href="assets/css/responsive.css">
        <link type="text/css" rel="stylesheet" href="assets/css/color.css">
        <!--link(type="text/css", rel='stylesheet', href='assets/css/color-1/color-1.css', id="color-skins")-->
        <link type="text/css" rel="stylesheet" href="#" id="color-skins">
        <script src="assets/libs/jquery/jquery-2.2.3.min.js"></script>
        <script src="assets/libs/js-cookie/js.cookie.js"></script>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/' + Cookies.get('color-skin') + '/' + 'color.css');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/color-1/color.css');
            }
        </script>
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
    </head>
    <body>
        <div class="body-wrapper">
            <!-- MENU MOBILE-->
            
            <?php include('mobile-menu.php');?>
            <!-- WRAPPER CONTENT-->
            <div class="wrapper-content">
                <!-- HEADER-->
                
                <?php include('header-2.php');?><!-- WRAPPER-->
                <div id="wrapper-content">
                    <!-- MAIN CONTENT-->
                    <div class="main-content">
                        <section class="page-banner hotel-view">
                            <div class="container">
                                <div class="page-title-wrapper">
                                    <div class="page-title-content">
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="index.html" class="link home">Home</a>
                                            </li>
                                            <li>
                                                <a href="hotel-result.html" class="link">Hotel</a>
                                            </li>
                                            <li class="active">
                                                <a href="#" class="link">matel Hotel</a>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <h2 class="captions">MATEL HOTEL</h2>
                                        <div class="price">
                                            <span class="text">from</span>
                                            <span class="number">30</span>
                                            <sup class="unit">$</sup>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="hotel-view-main padding-top padding-bottom">
                                <div class="container">
                                    <div class="journey-block">
                                        <h3 class="title-style-2">MATEL HOTEL
                                            <span> (sale off 30%)</span>
                                        </h3>
                                        <div class="wrapper-journey">
                                            <div class="item feature-item">
                                                <i class="icon-journey flaticon-people-3"></i>
                                                <p class="text">Fitness center</p>
                                            </div>
                                            <div class="item feature-item">
                                                <i class="icon-journey flaticon-cup"></i>
                                                <p class="text">Coffee shop</p>
                                            </div>
                                            <div class="item feature-item">
                                                <i class="icon-journey flaticon-food-2"></i>
                                                <p class="text">Restaurant</p>
                                            </div>
                                            <div class="item feature-item">
                                                <i class="icon-journey flaticon-people-4"></i>
                                                <p class="text">Baby care</p>
                                            </div>
                                            <div class="item feature-item">
                                                <i class="icon-journey flaticon-man"></i>
                                                <p class="text">Service room</p>
                                            </div>
                                            <div class="item feature-item">
                                                <i class="icon-journey flaticon-technology"></i>
                                                <p class="text">Wifi free</p>
                                            </div>
                                        </div>
                                        <div class="overview-block clearfix">
                                            <h3 class="title-style-3">Hotel Overview</h3>
                                            <div class="timeline-container">
                                                <div class="timeline timeline-hotel-view">
                                                    <div class="timeline-block">
                                                        <div class="timeline-title">
                                                            <span>Luxury Room</span>
                                                        </div>
                                                        <div class="timeline-point">
                                                            <i class="fa fa-circle-o"></i>
                                                        </div>
                                                        <div class="timeline-content">
                                                            <div class="row">
                                                                <div class="timeline-custom-col">
                                                                    <div class="image-hotel-view-block">
                                                                        <div class="slider-for group1">
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-2.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-2.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="slider-nav group1">
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-2.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-3.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-4.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-5.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-5.png" alt="">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="timeline-custom-col image-col hotels-layout">
                                                                    <div class="content-wrapper">
                                                                        <div class="content">
                                                                            <div class="title">
                                                                                <div class="price">
                                                                                    <sup>$</sup>
                                                                                    <span class="number">30</span>
                                                                                </div>
                                                                                <p class="for-price">for person per night</p>
                                                                            </div>
                                                                            <p class="text">Lorem ipsum dolor sit amet, consectetur elit. Nulla rhoncus lorem ipsum dolor consectete.</p>
                                                                            <div class="group-btn-tours">
                                                                                <a href="#" class="left-btn btn-book-tour">book now</a>
                                                                            </div>
                                                                        </div>
                                                                        <ul class="list-info list-unstyled">
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons hidden-icon fa fa-eye"></i>
                                                                                    <span class="number">234</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons hidden-icon fa fa-heart"></i>
                                                                                    <span class="number">156</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons hidden-icon fa fa-comment"></i>
                                                                                    <span class="number">19</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons fa fa-share-alt"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons fa fa-map-marker"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="timeline-custom-col full timeline-book-block">
                                                                    <div class="find-widget find-hotel-widget widget new-style">
                                                                        <h4 class="title-widgets">BOOK ROOM</h4>
                                                                        <form class="content-widget">
                                                                            <div class="text-input small-margin-top">
                                                                                <div class="input-daterange">
                                                                                    <div class="text-box-wrapper half">
                                                                                        <label class="tb-label">Check in</label>
                                                                                        <div class="input-group">
                                                                                            <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="text-box-wrapper half">
                                                                                        <label class="tb-label">Check out</label>
                                                                                        <div class="input-group">
                                                                                            <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="count adult-count text-box-wrapper">
                                                                                    <label class="tb-label">Adult</label>
                                                                                    <div class="select-wrapper">
                                                                                        <!--i.fa.fa-chevron-down-->
                                                                                        <select class="form-control custom-select selectbox">
                                                                                            <option selected="selected">1</option>
                                                                                            <option>2</option>
                                                                                            <option>3</option>
                                                                                            <option>4</option>
                                                                                            <option>5</option>
                                                                                            <option>6</option>
                                                                                            <option>7</option>
                                                                                            <option>8</option>
                                                                                            <option>9</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="count child-count text-box-wrapper">
                                                                                    <label class="tb-label">Child</label>
                                                                                    <div class="select-wrapper">
                                                                                        <!--i.fa.fa-chevron-down-->
                                                                                        <select class="form-control custom-select selectbox">
                                                                                            <option selected="selected">0</option>
                                                                                            <option>1</option>
                                                                                            <option>2</option>
                                                                                            <option>3</option>
                                                                                            <option>4</option>
                                                                                            <option>5</option>
                                                                                            <option>6</option>
                                                                                            <option>7</option>
                                                                                            <option>8</option>
                                                                                            <option>9</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="first-name text-box-wrapper">
                                                                                    <label class="tb-label">Your First Name</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your first name" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="last-name text-box-wrapper">
                                                                                    <label class="tb-label">Your Last Name</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your last name" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="email text-box-wrapper">
                                                                                    <label class="tb-label">Your Email</label>
                                                                                    <div class="input-group">
                                                                                        <input type="email" placeholder="Write your email address" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="phone text-box-wrapper">
                                                                                    <label class="tb-label">Your Number Phone</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your number phone" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="place text-box-wrapper">
                                                                                    <label class="tb-label">Where are your address?</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your address" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="note text-box-wrapper">
                                                                                    <label class="tb-label">Note:</label>
                                                                                    <div class="input-group">
                                                                                        <textarea placeholder="Write your note" rows="3" name="content" class="tb-input"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <button type="submit" data-hover="SEND REQUEST" class="btn btn-slide">
                                                                                    <span class="text">BOOK Now</span>
                                                                                    <span class="icons fa fa-long-arrow-right"></span>
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="timeline-block">
                                                        <div class="timeline-title">
                                                            <span>General Room</span>
                                                        </div>
                                                        <div class="timeline-point">
                                                            <i class="fa fa-circle-o"></i>
                                                        </div>
                                                        <div class="timeline-content">
                                                            <div class="row">
                                                                <div class="timeline-custom-col">
                                                                    <div class="image-hotel-view-block">
                                                                        <div class="slider-for group2">
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-2.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-2.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="slider-nav group2">
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-2.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-3.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-4.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-5.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-5.png" alt="">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="timeline-custom-col image-col hotels-layout">
                                                                    <div class="content-wrapper">
                                                                        <div class="content">
                                                                            <div class="title">
                                                                                <div class="price">
                                                                                    <sup>$</sup>
                                                                                    <span class="number">30</span>
                                                                                </div>
                                                                                <p class="for-price">for person per night</p>
                                                                            </div>
                                                                            <p class="text">Lorem ipsum dolor sit amet, consectetur elit. Nulla rhoncus lorem ipsum dolor consectete.</p>
                                                                            <div class="group-btn-tours">
                                                                                <a href="#" class="left-btn btn-book-tour">book now</a>
                                                                            </div>
                                                                        </div>
                                                                        <ul class="list-info list-unstyled">
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons hidden-icon fa fa-eye"></i>
                                                                                    <span class="number">234</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons hidden-icon fa fa-heart"></i>
                                                                                    <span class="number">156</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons hidden-icon fa fa-comment"></i>
                                                                                    <span class="number">19</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons fa fa-share-alt"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons fa fa-map-marker"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="timeline-custom-col full timeline-book-block">
                                                                    <div class="find-widget find-hotel-widget widget new-style">
                                                                        <h4 class="title-widgets">BOOK ROOM</h4>
                                                                        <form class="content-widget">
                                                                            <div class="text-input small-margin-top">
                                                                                <div class="input-daterange">
                                                                                    <div class="text-box-wrapper half">
                                                                                        <label class="tb-label">Check in</label>
                                                                                        <div class="input-group">
                                                                                            <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="text-box-wrapper half">
                                                                                        <label class="tb-label">Check out</label>
                                                                                        <div class="input-group">
                                                                                            <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="count adult-count text-box-wrapper">
                                                                                    <label class="tb-label">Adult</label>
                                                                                    <div class="select-wrapper">
                                                                                        <!--i.fa.fa-chevron-down-->
                                                                                        <select class="form-control custom-select selectbox">
                                                                                            <option selected="selected">1</option>
                                                                                            <option>2</option>
                                                                                            <option>3</option>
                                                                                            <option>4</option>
                                                                                            <option>5</option>
                                                                                            <option>6</option>
                                                                                            <option>7</option>
                                                                                            <option>8</option>
                                                                                            <option>9</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="count child-count text-box-wrapper">
                                                                                    <label class="tb-label">Child</label>
                                                                                    <div class="select-wrapper">
                                                                                        <!--i.fa.fa-chevron-down-->
                                                                                        <select class="form-control custom-select selectbox">
                                                                                            <option selected="selected">0</option>
                                                                                            <option>1</option>
                                                                                            <option>2</option>
                                                                                            <option>3</option>
                                                                                            <option>4</option>
                                                                                            <option>5</option>
                                                                                            <option>6</option>
                                                                                            <option>7</option>
                                                                                            <option>8</option>
                                                                                            <option>9</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="first-name text-box-wrapper">
                                                                                    <label class="tb-label">Your First Name</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your first name" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="last-name text-box-wrapper">
                                                                                    <label class="tb-label">Your Last Name</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your last name" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="email text-box-wrapper">
                                                                                    <label class="tb-label">Your Email</label>
                                                                                    <div class="input-group">
                                                                                        <input type="email" placeholder="Write your email address" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="phone text-box-wrapper">
                                                                                    <label class="tb-label">Your Number Phone</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your number phone" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="place text-box-wrapper">
                                                                                    <label class="tb-label">Where are your address?</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your address" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="note text-box-wrapper">
                                                                                    <label class="tb-label">Note:</label>
                                                                                    <div class="input-group">
                                                                                        <textarea placeholder="Write your note" rows="3" name="content" class="tb-input"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <button type="submit" data-hover="SEND REQUEST" class="btn btn-slide">
                                                                                    <span class="text">BOOK Now</span>
                                                                                    <span class="icons fa fa-long-arrow-right"></span>
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="timeline-block">
                                                        <div class="timeline-title">
                                                            <span>Family Room</span>
                                                        </div>
                                                        <div class="timeline-point">
                                                            <i class="fa fa-circle-o"></i>
                                                        </div>
                                                        <div class="timeline-content">
                                                            <div class="row">
                                                                <div class="timeline-custom-col">
                                                                    <div class="image-hotel-view-block">
                                                                        <div class="slider-for group3">
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-2.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-2.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/img-1.png" alt="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="slider-nav group3">
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-1.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-2.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-3.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-4.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-5.png" alt="">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="assets/images/hotel-view/thumb-5.png" alt="">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="timeline-custom-col image-col hotels-layout">
                                                                    <div class="content-wrapper">
                                                                        <div class="content">
                                                                            <div class="title">
                                                                                <div class="price">
                                                                                    <sup>$</sup>
                                                                                    <span class="number">30</span>
                                                                                </div>
                                                                                <p class="for-price">for person per night</p>
                                                                            </div>
                                                                            <p class="text">Lorem ipsum dolor sit amet, consectetur elit. Nulla rhoncus lorem ipsum dolor consectete.</p>
                                                                            <div class="group-btn-tours">
                                                                                <a href="#" class="left-btn btn-book-tour">book now</a>
                                                                            </div>
                                                                        </div>
                                                                        <ul class="list-info list-unstyled">
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons hidden-icon fa fa-eye"></i>
                                                                                    <span class="number">234</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons hidden-icon fa fa-heart"></i>
                                                                                    <span class="number">156</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons hidden-icon fa fa-comment"></i>
                                                                                    <span class="number">19</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons fa fa-share-alt"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="link">
                                                                                    <i class="icons fa fa-map-marker"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="timeline-custom-col full timeline-book-block">
                                                                    <div class="find-widget find-hotel-widget widget new-style">
                                                                        <h4 class="title-widgets">BOOK ROOM</h4>
                                                                        <form class="content-widget">
                                                                            <div class="text-input small-margin-top">
                                                                                <div class="input-daterange">
                                                                                    <div class="text-box-wrapper half">
                                                                                        <label class="tb-label">Check in</label>
                                                                                        <div class="input-group">
                                                                                            <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="text-box-wrapper half">
                                                                                        <label class="tb-label">Check out</label>
                                                                                        <div class="input-group">
                                                                                            <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="count adult-count text-box-wrapper">
                                                                                    <label class="tb-label">Adult</label>
                                                                                    <div class="select-wrapper">
                                                                                        <!--i.fa.fa-chevron-down-->
                                                                                        <select class="form-control custom-select selectbox">
                                                                                            <option selected="selected">1</option>
                                                                                            <option>2</option>
                                                                                            <option>3</option>
                                                                                            <option>4</option>
                                                                                            <option>5</option>
                                                                                            <option>6</option>
                                                                                            <option>7</option>
                                                                                            <option>8</option>
                                                                                            <option>9</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="count child-count text-box-wrapper">
                                                                                    <label class="tb-label">Child</label>
                                                                                    <div class="select-wrapper">
                                                                                        <!--i.fa.fa-chevron-down-->
                                                                                        <select class="form-control custom-select selectbox">
                                                                                            <option selected="selected">0</option>
                                                                                            <option>1</option>
                                                                                            <option>2</option>
                                                                                            <option>3</option>
                                                                                            <option>4</option>
                                                                                            <option>5</option>
                                                                                            <option>6</option>
                                                                                            <option>7</option>
                                                                                            <option>8</option>
                                                                                            <option>9</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="first-name text-box-wrapper">
                                                                                    <label class="tb-label">Your First Name</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your first name" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="last-name text-box-wrapper">
                                                                                    <label class="tb-label">Your Last Name</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your last name" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="email text-box-wrapper">
                                                                                    <label class="tb-label">Your Email</label>
                                                                                    <div class="input-group">
                                                                                        <input type="email" placeholder="Write your email address" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="phone text-box-wrapper">
                                                                                    <label class="tb-label">Your Number Phone</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your number phone" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="place text-box-wrapper">
                                                                                    <label class="tb-label">Where are your address?</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" placeholder="Write your address" class="tb-input">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="note text-box-wrapper">
                                                                                    <label class="tb-label">Note:</label>
                                                                                    <div class="input-group">
                                                                                        <textarea placeholder="Write your note" rows="3" name="content" class="tb-input"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <button type="submit" data-hover="SEND REQUEST" class="btn btn-slide">
                                                                                    <span class="text">BOOK Now</span>
                                                                                    <span class="icons fa fa-long-arrow-right"></span>
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="map-block">
                                    <div class="map-info">
                                        <h3 class="title-style-2">Contact Us</h3>
                                        <p class="address">
                                            <i class="fa fa-map-marker"></i> 333 Moo 10, Chert Wudthakas Road, Srikan, Don Mueang, Bangkok, Thailand</p>
                                        <p class="phone">
                                            <i class="fa fa-phone"></i> 910-740-6026</p>
                                        <p class="mail">
                                            <a href="mailto:domain@expooler.com">
                                                <i class="fa fa-envelope-o"></i>domain@expooler.com</a>
                                        </p>
                                        <div class="footer-block">
                                            <a class="btn btn-open-map">Open Map</a>
                                        </div>
                                    </div>
                                    <div id="googleMap"></div>
                                </div>
                                <div class="container">
                                    <div class="special-offer margin-top70">
                                        <h3 class="title-style-2">special offer</h3>
                                        <div class="special-offer-list">
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-17.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">alpha</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-18.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">otipus</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-19.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">sunrise</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-20.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">carisbean</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-17.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">alpha</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-18.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">otipus</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-19.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">sunrise</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-20.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">carisbean</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- BUTTON BACK TO TOP-->
                    <div id="back-top">
                        <a href="#top" class="link">
                            <i class="fa fa-angle-double-up"></i>
                        </a>
                    </div>
                </div>
                <!-- FOOTER-->
 
                <?php include('footer.php');?>

            </div>
        </div>
        <div class="theme-setting">
            <div class="theme-loading">
                <div class="theme-loading-content">
                    <div class="dots-loader"></div>
                </div>
            </div>
            <a href="javascript:;" class="btn-theme-setting">
                <i class="fa fa-tint"></i>
            </a>
            
        </div>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('.logo .header-logo img ,.logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-' + Cookies.get('color-skin') + '.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-' + Cookies.get('color-skin') + '.png');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('.logo .header-logo img , .logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-color-1.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-color-1.png');
            }
        </script>
        <!-- LIBRARY JS-->
        <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/libs/detect-browser/browser.js"></script>
        <script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
        <script src="assets/libs/wow-js/wow.min.js"></script>
        <script src="assets/libs/slick-slider/slick.min.js"></script>
        <script src="assets/libs/selectbox/js/jquery.selectbox-0.2.js"></script>
        <script src="assets/libs/please-wait/please-wait.min.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-thumbs.js"></script>
        <!--script(src="assets/libs/parallax/jquery.data-parallax.min.js")-->
        <!-- MAIN JS-->
        <script src="assets/js/main.js"></script>
        <!-- LOADING JS FOR PAGE-->
        <script src="assets/js/pages/hotel-view.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js"></script>
        <script src="assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script>
            var logo_str = 'assets/images/logo/logo-black-color-1.png';
            if (Cookies.set('color-skin'))
            {
                logo_str = 'assets/images/logo/logo-black-' + Cookies.set('color-skin') + '.png';
            }
            window.loading_screen = window.pleaseWait(
            {
                logo: logo_str,
                backgroundColor: '#fff',
                loadingHtml: "<div class='spinner sk-spinner-wave'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div>",
            });
        </script>
    </body>
</html>