<!DOCTYPE html>
<html lang="en">
<head>
        <title>Tripsathi | Car Rent</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome/css/font-awesome.css">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-flaticon/flaticon.css">
        <!-- LIBRARY CSS-->
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/animate/animate.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick-theme.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/selectbox/css/jquery.selectbox.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/please-wait/please-wait.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox8cbb.css?v=2.1.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-buttons3447.css?v=1.0.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-thumbsf2ad.css?v=1.0.7">
        <!-- STYLE CSS-->
        <link type="text/css" rel="stylesheet" href="assets/css/layout.css">
        <link type="text/css" rel="stylesheet" href="assets/css/components.css">
        <link type="text/css" rel="stylesheet" href="assets/css/responsive.css">
        <link type="text/css" rel="stylesheet" href="assets/css/color.css">
        <!--link(type="text/css", rel='stylesheet', href='assets/css/color-1/color-1.css', id="color-skins")-->
        <link type="text/css" rel="stylesheet" href="#" id="color-skins">
        <script src="assets/libs/jquery/jquery-2.2.3.min.js"></script>
        <script src="assets/libs/js-cookie/js.cookie.js"></script>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/' + Cookies.get('color-skin') + '/' + 'color.css');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/color-1/color.css');
            }
        </script>
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
    </head>
    <body>
        <div class="body-wrapper">
            <!-- MENU MOBILE-->
            
            <?php include('mobile-menu.php');?>
            <!-- WRAPPER CONTENT-->
            <div class="wrapper-content">
                <!-- HEADER-->
                
                <?php include('header-2.php');?>
                <!-- WRAPPER-->
                <div id="wrapper-content">
                    <!-- MAIN CONTENT-->
                    <div class="main-content">
                        <section class="page-banner car-rent-result">
                            <div class="container">
                                <div class="page-title-wrapper">
                                    <div class="page-title-content">
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="index.html" class="link home">Home</a>
                                            </li>
                                            <li class="active">
                                                <a href="#" class="link">car rent</a>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <h2 class="captions">car rent</h2>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="page-main">
                            <div class="trip-info">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="label-route-widget">
                                                <span class="departure">
                                                    <span class="city">Singapore, </span>
                                                    <span class="country">Singapore</span>
                                                </span>
                                                <i class="fa fa-long-arrow-right"></i>
                                                <span class="arrival">
                                                    <span class="city">Kuala Lumpur, </span>
                                                    <span class="country">Malaysia</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="label-time-widget">From
                                                <span class="departure">
                                                    <span class="date">6 March </span>at
                                                    <span class="hour">10:00</span>
                                                </span> to
                                                <span class="arrival">
                                                    <span class="date">9 March </span>at
                                                    <span class="hour">10:00</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="car-rent-result-main padding-top padding-bottom">
                                <div class="container">
                                    <div class="result-meta">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-12">
                                                <div class="result-count-wrapper">Results Found:
                                                    <span class="result-count">132</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-12">
                                                <div class="result-filter-wrapper">
                                                    <form>
                                                        <label class="result-filter-label">Sort by :</label>
                                                        <div class="selection-bar">
                                                            <div class="select-wrapper">
                                                                <select name="Recommended" class="custom-select selectbox">
                                                                    <option value="" disabled="disabled" selected="selected" hidden="hidden">Recommended</option>
                                                                    <option value="1">Recommended 1</option>
                                                                    <option value="2">Recommended 2</option>
                                                                </select>
                                                            </div>
                                                            <div class="select-wrapper">
                                                                <select name="Shortest" class="custom-select selectbox">
                                                                    <option value="" disabled="disabled" selected="selected" hidden="hidden">deals</option>
                                                                    <option value="1">deal 1</option>
                                                                    <option value="2">deal 2</option>
                                                                </select>
                                                            </div>
                                                            <div class="select-wrapper">
                                                                <select name="Nearby airport" class="custom-select selectbox">
                                                                    <!--option(value="" disabled selected hidden) price-->
                                                                    <option value="expensive">expensive</option>
                                                                    <option value="cheap">cheap</option>
                                                                </select>
                                                            </div>
                                                            <div class="select-wrapper">
                                                                <select name="Time" class="custom-select selectbox">
                                                                    <option value="" disabled="disabled" selected="selected" hidden="hidden">review score</option>
                                                                    <option value="5">5 stars</option>
                                                                    <option value="4">4 stars</option>
                                                                    <option value="3">3 stars</option>
                                                                    <option value="2">2 stars</option>
                                                                    <option value="1">1 stars</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="result-body">
                                        <div class="row">
                                            <div class="col-md-8 main-right">
                                                <div class="car-rent-list">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="car-rent-layout">
                                                                <div class="image-wrapper">
                                                                    <a href="tour-view.html" class="link">
                                                                        <img src="assets/images/cars/car-1.jpg" alt="" class="img-responsive">
                                                                    </a>
                                                                </div>
                                                                <div class="content-wrapper">
                                                                    <a href="#" class="title">mercedes c200</a>
                                                                    <div class="price">
                                                                        <sup>$</sup>
                                                                        <span class="number">250</span>
                                                                        <p class="for-price">per day</p>
                                                                    </div>
                                                                    <div class="sub-title">luxury</div>
                                                                    <p class="text">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                    <a
                                                                        href="tour-view.html" class="btn btn-gray">book now</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="car-rent-layout">
                                                                <div class="image-wrapper">
                                                                    <a href="tour-view.html" class="link">
                                                                        <img src="assets/images/cars/car-2.jpg" alt="" class="img-responsive">
                                                                    </a>
                                                                </div>
                                                                <div class="content-wrapper">
                                                                    <a href="#" class="title">toyota 86</a>
                                                                    <div class="price">
                                                                        <sup>$</sup>
                                                                        <span class="number">150</span>
                                                                        <p class="for-price">per day</p>
                                                                    </div>
                                                                    <div class="sub-title">luxury</div>
                                                                    <p class="text">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                    <a
                                                                        href="tour-view.html" class="btn btn-gray">book now</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="car-rent-layout">
                                                                <div class="image-wrapper">
                                                                    <a href="tour-view.html" class="link">
                                                                        <img src="assets/images/cars/car-3.jpg" alt="" class="img-responsive">
                                                                    </a>
                                                                </div>
                                                                <div class="content-wrapper">
                                                                    <a href="#" class="title">camry 2.5q</a>
                                                                    <div class="price">
                                                                        <sup>$</sup>
                                                                        <span class="number">320</span>
                                                                        <p class="for-price">per day</p>
                                                                    </div>
                                                                    <div class="sub-title">luxury</div>
                                                                    <p class="text">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                    <a
                                                                        href="tour-view.html" class="btn btn-gray">book now</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="car-rent-layout">
                                                                <div class="image-wrapper">
                                                                    <a href="tour-view.html" class="link">
                                                                        <img src="assets/images/cars/car-4.jpg" alt="" class="img-responsive">
                                                                    </a>
                                                                </div>
                                                                <div class="content-wrapper">
                                                                    <a href="#" class="title">land cruiser</a>
                                                                    <div class="price">
                                                                        <sup>$</sup>
                                                                        <span class="number">150</span>
                                                                        <p class="for-price">per day</p>
                                                                    </div>
                                                                    <div class="sub-title">luxury</div>
                                                                    <p class="text">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                    <a
                                                                        href="tour-view.html" class="btn btn-gray">book now</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="car-rent-layout">
                                                                <div class="image-wrapper">
                                                                    <a href="tour-view.html" class="link">
                                                                        <img src="assets/images/cars/car-5.jpg" alt="" class="img-responsive">
                                                                    </a>
                                                                </div>
                                                                <div class="content-wrapper">
                                                                    <a href="#" class="title">innova g</a>
                                                                    <div class="price">
                                                                        <sup>$</sup>
                                                                        <span class="number">230</span>
                                                                        <p class="for-price">per day</p>
                                                                    </div>
                                                                    <div class="sub-title">luxury</div>
                                                                    <p class="text">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                    <a
                                                                        href="tour-view.html" class="btn btn-gray">book now</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="car-rent-layout">
                                                                <div class="image-wrapper">
                                                                    <a href="tour-view.html" class="link">
                                                                        <img src="assets/images/cars/car-6.jpg" alt="" class="img-responsive">
                                                                    </a>
                                                                </div>
                                                                <div class="content-wrapper">
                                                                    <a href="#" class="title">vios j</a>
                                                                    <div class="price">
                                                                        <sup>$</sup>
                                                                        <span class="number">124</span>
                                                                        <p class="for-price">per day</p>
                                                                    </div>
                                                                    <div class="sub-title">luxury</div>
                                                                    <p class="text">Lorem ipsum dolor sit amet, consectetur. Nulla rhoncus ultrices purus, volutpat. Lorem ipsum dolor sit amet, consectetur elit dolor sit amet, consectetur nulla rhoncus ultrices purus.</p>
                                                                    <a
                                                                        href="tour-view.html" class="btn btn-gray">book now</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <nav class="pagination-list margin-top70">
                                                    <ul class="pagination">
                                                        <li>
                                                            <a href="#" aria-label="Previous" class="btn-pagination previous">
                                                                <span aria-hidden="true" class="fa fa-angle-left"></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="btn-pagination active">01</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="btn-pagination">02</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="btn-pagination">03</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="btn-pagination">...</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" aria-label="Next" class="btn-pagination next">
                                                                <span aria-hidden="true" class="fa fa-angle-right"></span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                            <div class="col-md-4 sidebar-widget">
                                                <div class="col-2">
                                                    <div class="find-widget find-flight-widget widget">
                                                        <h4 class="title-widgets">find your car</h4>
                                                        <form class="content-widget">
                                                            <div class="ffw-radio-selection">
                                                                <span class="ffw-radio-btn-wrapper">
                                                                    <input type="radio" name="flight type" value="one way" id="flight-type-1" checked="checked" class="ffw-radio-btn">
                                                                    <label for="flight-type-1" class="ffw-radio-label">One Way</label>
                                                                </span>
                                                                <span class="ffw-radio-btn-wrapper">
                                                                    <input type="radio" name="flight type" value="round trip" id="flight-type-2" class="ffw-radio-btn">
                                                                    <label for="flight-type-2" class="ffw-radio-label">Round Trip</label>
                                                                </span>
                                                                <span class="ffw-radio-btn-wrapper">
                                                                    <input type="radio" name="flight type" value="multiple cities" id="flight-type-3" class="ffw-radio-btn">
                                                                    <label for="flight-type-3" class="ffw-radio-label">Multiple Cities</label>
                                                                </span>
                                                                <div class="stretch">&nbsp;</div>
                                                            </div>
                                                            <div class="text-input small-margin-top">
                                                                <div class="text-box-wrapper">
                                                                    <label class="tb-label">Where do you want to go?</label>
                                                                    <div class="input-group">
                                                                        <input type="text" placeholder="Write the place" class="tb-input">
                                                                    </div>
                                                                </div>
                                                                <div class="input-daterange">
                                                                    <div class="text-box-wrapper half left">
                                                                        <label class="tb-label">Check in</label>
                                                                        <div class="input-group">
                                                                            <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="text-box-wrapper half right">
                                                                        <label class="tb-label">Check out</label>
                                                                        <div class="input-group">
                                                                            <input type="text" placeholder="MM/DD/YY" class="tb-input">
                                                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="text-box-wrapper half left">
                                                                    <label class="tb-label">Number of Adult</label>
                                                                    <div class="input-group">
                                                                        <button disabled="disabled" data-type="minus" data-field="quant[1]" class="input-group-btn btn-minus">
                                                                            <span class="tb-icon fa fa-minus"></span>
                                                                        </button>
                                                                        <input type="number" name="quant[1]" min="1" max="9" value="1" class="tb-input count">
                                                                        <button data-type="plus" data-field="quant[1]" class="input-group-btn btn-plus">
                                                                            <span class="tb-icon fa fa-plus"></span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div class="text-box-wrapper half right">
                                                                    <label class="tb-label">Number of Child</label>
                                                                    <div class="input-group">
                                                                        <button disabled="disabled" data-type="minus" data-field="quant[2]" class="input-group-btn btn-minus">
                                                                            <span class="tb-icon fa fa-minus"></span>
                                                                        </button>
                                                                        <input type="number" name="quant[2]" min="0" max="9" value="0" class="tb-input count">
                                                                        <button data-type="plus" data-field="quant[2]" class="input-group-btn btn-plus">
                                                                            <span class="tb-icon fa fa-plus"></span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="submit" data-hover="SEND NOW" class="btn btn-slide small-margin-top">
                                                                <span class="text">search now</span>
                                                                <span class="icons fa fa-long-arrow-right"></span>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="col-2">
                                                    <div class="col-1">
                                                        <div class="price-widget widget">
                                                            <div class="title-widget">
                                                                <div class="title">price</div>
                                                            </div>
                                                            <div class="content-widget">
                                                                <div class="price-wrapper">
                                                                    <div data-range_min="0" data-range_max="3000" data-cur_min="450" data-cur_max="1800" class="nstSlider">
                                                                        <div class="leftGrip indicator">
                                                                            <div class="number"></div>
                                                                        </div>
                                                                        <div class="rightGrip indicator">
                                                                            <div class="number"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="leftLabel">0</div>
                                                                    <div class="rightLabel">3000</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="turkey-cities-widget widget">
                                                            <div class="title-widget">
                                                                <div class="title">rating</div>
                                                            </div>
                                                            <div class="content-widget">
                                                                <form class="radio-selection">
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="rating" value="1stars" id="1stars" class="radio-btn">
                                                                        <label for="1stars" class="radio-label stars stars5">1stars</label>
                                                                        <span class="count">27</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="rating" value="2stars" id="2stars" class="radio-btn">
                                                                        <label for="2stars" class="radio-label stars stars4">2stars</label>
                                                                        <span class="count">75</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="rating" value="3stars" id="3stars" class="radio-btn">
                                                                        <label for="3stars" class="radio-label stars stars3">3stars</label>
                                                                        <span class="count">35</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="rating" value="4stars" id="4stars" class="radio-btn">
                                                                        <label for="4stars" class="radio-label stars stars2">4stars</label>
                                                                        <span class="count">34</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="rating" value="5stars" id="5stars" class="radio-btn">
                                                                        <label for="5stars" class="radio-label stars stars1">5stars</label>
                                                                        <span class="count">65</span>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1">
                                                        <div class="car-type-widget widget">
                                                            <div class="title-widget">
                                                                <div class="title">Car type</div>
                                                            </div>
                                                            <div class="content-widget">
                                                                <form class="radio-selection">
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="car-type" value="Economy" id="Economy" class="radio-btn">
                                                                        <label for="Economy" class="radio-label">Economy</label>
                                                                        <span class="count">27</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="car-type" value="Compact" id="Compact" class="radio-btn">
                                                                        <label for="Compact" class="radio-label">Compact</label>
                                                                        <span class="count">75</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="car-type" value="Midsize" id="Midsize" class="radio-btn">
                                                                        <label for="Midsize" class="radio-label">Midsize</label>
                                                                        <span class="count">35</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="car-type" value="Standard" id="Standard" class="radio-btn">
                                                                        <label for="Standard" class="radio-label">Standard</label>
                                                                        <span class="count">34</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="car-type" value="Premium" id="Premium" class="radio-btn">
                                                                        <label for="Premium" class="radio-label">Premium</label>
                                                                        <span class="count">65</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="car-type" value="Fullsize" id="Fullsize" class="radio-btn">
                                                                        <label for="Fullsize" class="radio-label">Fullsize</label>
                                                                        <span class="count">65</span>
                                                                    </div>
                                                                    <div class="radio-btn-wrapper">
                                                                        <input type="radio" name="car-type" value="Convertible" id="Convertible" class="radio-btn">
                                                                        <label for="Convertible" class="radio-label">Convertible</label>
                                                                        <span class="count">65</span>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-2">
                                                    <div class="special-equipment-widget widget">
                                                        <div class="title-widget">
                                                            <div class="title">special equipment</div>
                                                        </div>
                                                        <div class="content-widget">
                                                            <form class="radio-selection">
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="equipment" value="Infant-Seat" id="Infant-Seat" class="radio-btn">
                                                                    <label for="Infant-Seat" class="radio-label">Infant Seat</label>
                                                                    <span class="count">27</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="equipment" value="Tooldler-Seat" id="Tooldler-Seat" class="radio-btn">
                                                                    <label for="Tooldler-Seat" class="radio-label">Tooldler Seat</label>
                                                                    <span class="count">75</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="equipment" value="Navigation-System" id="Navigation-System" class="radio-btn">
                                                                    <label for="Navigation-System" class="radio-label">Navigation System</label>
                                                                    <span class="count">35</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="equipment" value="Standard2" id="Standard2" class="radio-btn">
                                                                    <label for="Standard2" class="radio-label">Standard</label>
                                                                    <span class="count">34</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="equipment" value="Ski-Rack" id="Ski-Rack" class="radio-btn">
                                                                    <label for="Ski-Rack" class="radio-label">Ski Rack</label>
                                                                    <span class="count">65</span>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="suppliers-widget widget">
                                                        <div class="title-widget">
                                                            <div class="title">suppliers</div>
                                                        </div>
                                                        <div class="content-widget">
                                                            <form class="radio-selection">
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="suppliers" value="National" id="National" class="radio-btn">
                                                                    <label for="National" class="radio-label">National</label>
                                                                    <span class="count">27</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="suppliers" value="Dollar" id="Dollar" class="radio-btn">
                                                                    <label for="Dollar" class="radio-label">Dollar</label>
                                                                    <span class="count">75</span>
                                                                </div>
                                                                <div class="radio-btn-wrapper">
                                                                    <input type="radio" name="suppliers" value="Alamo" id="Alamo" class="radio-btn">
                                                                    <label for="Alamo" class="radio-label">Alamo</label>
                                                                    <span class="count">35</span>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="special-offer margin-top70">
                                        <h3 class="title-style-2">special offer</h3>
                                        <div class="special-offer-list">
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-13.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">alpha</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-14.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">otipus</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-15.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">sunrise</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-16.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">carisbean</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-13.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">alpha</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-14.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">otipus</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-15.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">sunrise</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="special-offer-layout">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="assets/images/footer/offer-16.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">carisbean</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BUTTON BACK TO TOP-->
                    <div id="back-top">
                        <a href="#top" class="link">
                            <i class="fa fa-angle-double-up"></i>
                        </a>
                    </div>
                </div>
                <!-- FOOTER-->

                <?php include('footer.php');?>

            </div>
        </div>
        <div class="theme-setting">
            <div class="theme-loading">
                <div class="theme-loading-content">
                    <div class="dots-loader"></div>
                </div>
            </div>
            <a href="javascript:;" class="btn-theme-setting">
                <i class="fa fa-tint"></i>
            </a>
            
        </div>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('.logo .header-logo img ,.logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-' + Cookies.get('color-skin') + '.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-' + Cookies.get('color-skin') + '.png');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('.logo .header-logo img , .logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-color-1.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-color-1.png');
            }
        </script>
        <!-- LIBRARY JS-->
        <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/libs/detect-browser/browser.js"></script>
        <script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
        <script src="assets/libs/wow-js/wow.min.js"></script>
        <script src="assets/libs/slick-slider/slick.min.js"></script>
        <script src="assets/libs/selectbox/js/jquery.selectbox-0.2.js"></script>
        <script src="assets/libs/please-wait/please-wait.min.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-thumbs.js"></script>
        <!--script(src="assets/libs/parallax/jquery.data-parallax.min.js")-->
        <!-- MAIN JS-->
        <script src="assets/js/main.js"></script>
        <!-- LOADING JS FOR PAGE-->
        <script src="assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="assets/libs/nst-slider/js/jquery.nstSlider.min.js"></script>
        <script src="assets/libs/plus-minus-input/plus-minus-input.js"></script>
        <script src="assets/js/pages/sidebar.js"></script>
        <script src="assets/js/pages/result.js"></script>
        <script>
            var logo_str = 'assets/images/logo/logo-black-color-1.png';
            if (Cookies.set('color-skin'))
            {
                logo_str = 'assets/images/logo/logo-black-' + Cookies.set('color-skin') + '.png';
            }
            window.loading_screen = window.pleaseWait(
            {
                logo: logo_str,
                backgroundColor: '#fff',
                loadingHtml: "<div class='spinner sk-spinner-wave'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div>",
            });
        </script>
    </body>
</html>