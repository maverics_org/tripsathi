<nav class="navigation">
    <ul class="nav-links nav navbar-nav">
        <li class="dropdown active">
            <a href="javascript:void(0)" class="main-menu">
                <span class="text">Home</span>
                <!--<span class="icons-dropdown">
                    <i class="fa fa-angle-down"></i>
                </span>-->
            </a>
<!---            <ul class="dropdown-menu dropdown-menu-1">
                <li><a href="index" class="link-page">Home</a></li>
              <li><a href="homepage-02.php" class="link-page">Homepage 02</a></li>
                <li><a href="homepage-03.php" class="link-page">Homepage 03</a></li>
                <li><a href="homepage-04.php" class="link-page">Homepage 04</a></li>
            </ul>-->
        </li>

        <li class="dropdown">
            <a href="javascript:void(0)" class="main-menu">
                <span class="text">Cars</span>
                <span class="icons-dropdown">
                    <i class="fa fa-angle-down"></i>
                </span>
            </a>
            <ul class="dropdown-menu dropdown-menu-1">
                <li><a href="car-rent-result.php" class="link-page">Car result</a></li>
                <li><a href="car-detail.php" class="link-page">Car detail</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="javascript:void(0)" class="main-menu">
                <span class="text">Tours</span>
                <span class="icons-dropdown">
                    <i class="fa fa-angle-down"></i>
                </span>
            </a>
            <ul class="dropdown-menu dropdown-menu-1">
                <li><a href="tour-result" class="link-page">Tour result</a></li>
                <li><a href="tour-view" class="link-page">Tour view</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="javascript:void(0)" class="main-menu">
                <span class="text">Flights</span>
                <span class="icons-dropdown">
                    <i class="fa fa-angle-down"></i>
                </span>
            </a>
            <ul class="dropdown-menu dropdown-menu-1">
                <li><a href="car-rent-result.php" class="link-page">Flight result</a></li>
                <li><a href="car-detail.php" class="link-page">Flight detail</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="javascript:void(0)" class="main-menu">
                <span class="text">Hotels</span>
                    <span class="icons-dropdown">
                        <i class="fa fa-angle-down"></i>
                    </span>
            </a>
            <ul class="dropdown-menu dropdown-menu-1">
                <li><a href="hotel-result.php" class="link-page">Hotel result</a></li>
                <li><a href="hotel-view.php" class="link-page">Hotel view</a></li>
            </ul>
        </li>
        
        
        
        <li class="dropdown">
            <a href="javascript:void(0)" class="main-menu">
                <span class="text">Blogs</span>
                <span class="icons-dropdown">
                    <i class="fa fa-angle-down"></i>
                </span>
            </a>
            <ul class="dropdown-menu dropdown-menu-1">
                <li><a href="blog.php" class="link-page">blog list</a></li>
                <li><a href="blog-detail.php" class="link-page">blog detail</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="javascript:void(0)" class="main-menu">
                <span class="text">Introduction</span>
                <span class="icons-dropdown">
                    <i class="fa fa-angle-down"></i>
                </span>
            </a>
            <ul class="dropdown-menu dropdown-menu-1">
                <li><a href="about-us.php" class="link-page">About Us</a></li>
                <li><a href="team-detail.php" class="link-page">Team detail</a></li>
                <li><a href="car-detail.php" class="link-page">Privacy Policy</a></li>
                <li><a href="cruises-result.php" class="link-page">Return Policy</a></li>
                <li><a href="cruises-detail.php" class="link-page">Terms &amp; Conditions</a></li>
                
                 <li><a href="career.php" class="link-page">Career</a></li>
                <li><a href="faq.php" class="link-page">faq</a></li>
            </ul>
        </li>
        <li>
            <a href="contact.php" class="main-menu">
                <span class="text">contact</span>
            </a>
        </li>
        <li class="button-search">
            <p class="main-menu">
                <i class="fa fa-search"></i>
            </p>
        </li>
        </ul>
        <div class="nav-search hide">
            <form>
                <input type="text" placeholder="Search" class="searchbox" />
                <button type="submit" class="searchbutton fa fa-search"></button>
            </form>
        </div>
</nav>