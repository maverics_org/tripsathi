<header>
    <div class="bg-transparent header-01">
        <div class="header-topbar">
            <?php include('top-header.php');?>
        </div>
    <div class="header-main">
        <div class="container">
            <div class="header-main-wrapper">
                <div class="hamburger-menu">
                    <div class="hamburger-menu-wrapper">
                        <div class="icons"></div>
                    </div>
                </div>
                <div class="navbar-header">
                    <div class="logo">
                        <a href="index" class="header-logo">
                            <img src="assets/images/logo/logo-white-color-11.png" alt="" />
                        </a>
                    </div>
                </div>
                <?php include('navigation.php');?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    </div>
</header>