<footer>
                    <div class="footer-main padding-top padding-bottom">
                        <div class="container">
                            <div class="footer-main-wrapper">
                                <a href="index.php" class="logo-footer">
                                    <img src="assets/images/logo/logo-white-color-11.png" alt="" class="img-responsive" />
                                </a>
                                <div class="row">
                                    <div class="col-2">
                                        <div class="col-md-3 col-xs-5">
                                            <div class="contact-us-widget widget">
                                                <div class="title-widget">contact us</div>
                                                <div class="content-widget">
                                                    <div class="info-list">
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <i class="icons fa fa-map-marker"></i>
                                                                <a href="#" class="link">Opp. Old Custom Office, Wilsonia College Road, Civil Lines Moradabad-244001</a>
                                                            </li>
                                                            <li>
                                                                <i class="icons fa fa-phone"></i>
                                                                <a href="#" class="link">+91-6395016943</a>
                                                            </li>
                                                            <li>
                                                                <i class="icons fa fa-envelope-o"></i>
                                                                <a href="#" class="link">info@tripsathi.in</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="form-email">
                                                        <p class="text">Sign up for our mailing list to get latest updates and offers.</p>
                                                        <form action="http://swlabs.co/exploore/index.html">
                                                            <div class="input-group">
                                                                <input type="text" placeholder="Email address" class="form-control form-email-widget" />
                                                                <span class="input-group-btn">
                                                                    <button type="submit" class="btn-email">&#10004;</button>
                                                                </span>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <div class="booking-widget widget text-center">
                                                <div class="title-widget">book now</div>
                                                <div class="content-widget">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="#" class="link">Flights</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Cars</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Hotels</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Privacy Policy</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Terms &amp; Conditions</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-4">
                                            <div class="explore-widget widget">
                                                <div class="title-widget">explore</div>
                                                <div class="content-widget">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="#" class="link">Local Car Rentals</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Outstation Taxi</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">One way Cabs</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Two Way Taxi</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Airport Taxi</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Cabs By City</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="col-md-2 col-xs-6">
                                            <div class="top-deals-widget widget">
                                                <div class="title-widget">Cabs in Moradabad</div>
                                                <div class="content-widget">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="#" class="link">Moradabad to Amroha</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Moradabad to Manali</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Moradabad to Rishikesh</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Moradabad to Noida</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Moradabad to New Delhi</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="link">Moradabad to Dehradoon</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <div class="destination-widget widget">
                                                <div class="title-widget">Destination</div>
                                                <div class="content-widget">
                                                    <ul class="list-unstyled list-inline">
                                                        <li>
                                                            <a href="#" class="thumb">
                                                                <img src="assets/images/footer/gallery-01.jpg" alt="" class="img-responsive" />
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="thumb">
                                                                <img src="assets/images/footer/gallery-02.jpg" alt="" class="img-responsive" />
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="thumb">
                                                                <img src="assets/images/footer/gallery-03.jpg" alt="" class="img-responsive" />
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="thumb">
                                                                <img src="assets/images/footer/gallery-04.jpg" alt="" class="img-responsive" />
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="thumb">
                                                                <img src="assets/images/footer/gallery-05.jpg" alt="" class="img-responsive" />
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="thumb">
                                                                <img src="assets/images/footer/gallery-06.jpg" alt="" class="img-responsive" />
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hyperlink">
                        <div class="container">
                            <div class="slide-logo-wrapper">
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-01.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-02.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-03.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-04.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-05.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-06.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-01.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-02.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-03.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-04.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-05.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                                <div class="logo-item">
                                    <a href="#" class="link">
                                        <img src="assets/images/footer/logo-06.png" alt="" class="img-responsive" />
                                    </a>
                                </div>
                            </div>
                            <div class="social-footer">
                                <ul class="list-inline list-unstyled">
                                    <li>
                                        <a href="#" class="link facebook">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="link twitter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="link pinterest">
                                            <i class="fa fa-pinterest-p"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="link google">
                                            <i class="fa fa-google"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="name-company">Copyright &copy; 2018 | Developed by : Maverics Techinclusive Pvt. Ltd.</div>
                        </div>
                    </div>
                </footer>