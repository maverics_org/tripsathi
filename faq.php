<!DOCTYPE html>
<html lang="en">
 <head>
        <title>Tripsathi | faq</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome/css/font-awesome.css">
        <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-flaticon/flaticon.css">
        <!-- LIBRARY CSS-->
        <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/animate/animate.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/slick-slider/slick-theme.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/selectbox/css/jquery.selectbox.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/please-wait/please-wait.css">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox8cbb.css?v=2.1.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-buttons3447.css?v=1.0.5">
        <link type="text/css" rel="stylesheet" href="assets/libs/fancybox/css/jquery.fancybox-thumbsf2ad.css?v=1.0.7">
        <!-- STYLE CSS-->
        <link type="text/css" rel="stylesheet" href="assets/css/layout.css">
        <link type="text/css" rel="stylesheet" href="assets/css/components.css">
        <link type="text/css" rel="stylesheet" href="assets/css/responsive.css">
        <link type="text/css" rel="stylesheet" href="assets/css/color.css">
        <!--link(type="text/css", rel='stylesheet', href='assets/css/color-1/color-1.css', id="color-skins")-->
        <link type="text/css" rel="stylesheet" href="#" id="color-skins">
        <script src="assets/libs/jquery/jquery-2.2.3.min.js"></script>
        <script src="assets/libs/js-cookie/js.cookie.js"></script>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/' + Cookies.get('color-skin') + '/' + 'color.css');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('#color-skins').attr('href', 'assets/css/color-1/color.css');
            }
        </script>
    </head>
    <body>
        <div class="body-wrapper">
            <!-- MENU MOBILE-->
            
            <?php include('mobile-menu.php');?>            <!-- WRAPPER CONTENT-->
            <div class="wrapper-content">
                <!-- HEADER-->
                
                <?php include('header-2.php');?>
                <!-- WRAPPER-->
                <div id="wrapper-content">
                    <!-- MAIN CONTENT-->
                    <div class="main-content">
                        <section class="page-banner bg-faq">
                            <div class="container">
                                <div class="page-title-wrapper">
                                    <div class="page-title-content">
                                        <ol class="breadcrumb">
                                            <li>
                                                <a href="index.html" class="link home">Home</a>
                                            </li>
                                            <li>
                                                <a href="#" class="link">page</a>
                                            </li>
                                            <li class="active">
                                                <a href="#" class="link">faq</a>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <h2 class="captions">FAQ</h2>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="page-main">
                            <div class="section-faq padding-top padding-bottom">
                                <div class="container">
                                    <div class="wrapper-faq">
                                        <div class="row">
                                            <div class="col-md-8 col-sm-12 col-xs-12 main-right">
                                                <div class="wrapper-content-faq">
                                                    <div class="content-faq padding-bottom">
                                                        <h3 class="title-style-2">CHOOSING A Hotel</h3>
                                                        <div id="accordion-1" class="panel-group wrapper-accordion">
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1-1" aria-expanded="false" class="accordion-toggle collapsed"> Travelling with childrend</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-1-1" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1-2" aria-expanded="false" class="accordion-toggle collapsed"> Travelling in a group</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-1-2" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1-3" aria-expanded="false" class="accordion-toggle collapsed"> Travelling solo</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-1-3" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1-4" aria-expanded="false" class="accordion-toggle collapsed"> Food, drink and dietary requirements</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-1-4" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1-5" aria-expanded="false" class="accordion-toggle collapsed"> Safety and medical information</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-1-5" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-faq padding-bottom">
                                                        <h3 class="title-style-2">Flight Booking </h3>
                                                        <div id="accordion-2" class="panel-group wrapper-accordion">
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2-1" aria-expanded="false" class="accordion-toggle collapsed"> Travel Insurance</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-2-1" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2-2" aria-expanded="false" class="accordion-toggle collapsed"> Visa Information</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-2-2" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2-3" aria-expanded="false" class="accordion-toggle collapsed"> Currency information</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-2-3" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2-4" aria-expanded="false" class="accordion-toggle collapsed"> What should I bring?</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-2-4" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2-5" aria-expanded="false" class="accordion-toggle collapsed"> Financial security</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-2-5" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-faq">
                                                        <h3 class="title-style-2">ON YOUR TRIP</h3>
                                                        <div id="accordion-3" class="panel-group wrapper-accordion">
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3-1" aria-expanded="false" class="accordion-toggle collapsed"> Arrival information</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-3-1" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3-2" aria-expanded="false" class="accordion-toggle collapsed"> Tipping information</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-3-2" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3-3" aria-expanded="false" class="accordion-toggle collapsed"> Keeping in touch with home</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-3-3" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                            <div class="panel">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3-4" aria-expanded="false" class="accordion-toggle collapsed"> Accommodation and Transport</a>
                                                                    </h5>
                                                                </div>
                                                                <div id="collapse-3-4" role="tabpanel" aria-expanded="false" style="height: 0px" class="panel-collapse collapse">
                                                                    <div class="panel-body">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                                                        ea commodo consequat duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute. Consectetur adipisicing elit, sed do eiusmod
                                                                        tempor incididunt ut labore et dolore magna aliqua.</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12 col-xs-12 wrapper-contact-faq">
                                                <div class="contact-wrapper">
                                                    <div class="contact-box">
                                                        <h5 class="title">send your request</h5>
                                                        <form class="contact-form">
                                                            <input type="text" placeholder="Name" class="form-control form-input">
                                                            <!--label.control-label.form-label.warning-label(for="") Warning for the above !-->
                                                            <input type="email" placeholder="Email" class="form-control form-input">
                                                            <!--label.control-label.form-label.warning-label(for="") Warning for the above !-->
                                                            <textarea placeholder="Question" class="form-control form-input"></textarea>
                                                            <div class="contact-submit">
                                                                <button type="submit" data-hover="SEND NOW" class="btn btn-slide">
                                                                    <span class="text">send question</span>
                                                                    <span class="icons fa fa-long-arrow-right"></span>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BUTTON BACK TO TOP-->
                    <div id="back-top">
                        <a href="#top" class="link">
                            <i class="fa fa-angle-double-up"></i>
                        </a>
                    </div>
                </div>
                <!-- FOOTER-->
    
                <?php include('footer.php');?>
            </div>
        </div>
        <div class="theme-setting">
            <div class="theme-loading">
                <div class="theme-loading-content">
                    <div class="dots-loader"></div>
                </div>
            </div>
            <a href="javascript:;" class="btn-theme-setting">
                <i class="fa fa-tint"></i>
            </a>
            
        </div>
        <script>
            if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1'))
            {
                $('.logo .header-logo img ,.logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-' + Cookies.get('color-skin') + '.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-' + Cookies.get('color-skin') + '.png');
            }
            else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1'))
            {
                $('.logo .header-logo img , .logo-footer img, .group-logo .img-logo').attr('src', 'assets/images/logo/logo-white-color-1.png');
                $('.logo-black img').attr('src', 'assets/images/logo/logo-black-color-1.png');
            }
        </script>
        <!-- LIBRARY JS-->
        <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/libs/detect-browser/browser.js"></script>
        <script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
        <script src="assets/libs/wow-js/wow.min.js"></script>
        <script src="assets/libs/slick-slider/slick.min.js"></script>
        <script src="assets/libs/selectbox/js/jquery.selectbox-0.2.js"></script>
        <script src="assets/libs/please-wait/please-wait.min.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-thumbs.js"></script>
        <!--script(src="assets/libs/parallax/jquery.data-parallax.min.js")-->
        <!-- MAIN JS-->
        <script src="assets/js/main.js"></script>
        <!-- LOADING JS FOR PAGE-->
        <script src="assets/js/pages/faq.js"></script>
        <script>
            var logo_str = 'assets/images/logo/logo-black-color-1.png';
            if (Cookies.set('color-skin'))
            {
                logo_str = 'assets/images/logo/logo-black-' + Cookies.set('color-skin') + '.png';
            }
            window.loading_screen = window.pleaseWait(
            {
                logo: logo_str,
                backgroundColor: '#fff',
                loadingHtml: "<div class='spinner sk-spinner-wave'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div>",
            });
        </script>
    </body>
</html>