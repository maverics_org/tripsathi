<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="flot.php">Flot Charts</a></li>
                                <li><a href="morris.php">Morris.js Charts</a></li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li><a><i class="fa fa-users fa-fw"></i> All Users<span class="fa arrow"></span></a>
                        
                        <ul class="nav nav-second-level">
                                <li><a href="#">Cab Customers</a></li>
                                <li><a href="#">Tour Package Customers</a></li>
                                <li><a href="#">Flight Customers</a></li>
                                <li><a href="#">Hotels Customers</a></li>
                            </ul>
                        </li>
                        <li><a href="tables.php"><i class="fa fa-sitemap fa-fw"></i>Tour Booking<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="add-booking.php">Add Tour Package</a></li>
                                <li><a href="view-package.php">View Tour Package</a></li>
                                <li><a href="view-booking.php">View Tour Booking </a></li>
                            </ul>
                        </li>
                        <li><a href="tables.php"><i class="fa fa-car fa-fw"></i> Cab Booking</a></li>
                        <li><a href="tables.php"><i class="fa fa-ticket fa-fw"></i> Support Ticket</a></li>
                        <li><a href="tables.php"><i class="fa fa-bank fa-fw"></i> Hotel Booking</a></li>
                        
                        <li><a href="tables.php"><i class="fa fa-plane fa-fw"></i> Flight Booking</a></li>
                        <li><a href="forms.php"><i class="fa fa-edit fa-fw"></i> Forms</a></li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.php">Panels and Wells</a>
                                </li>
                                <li>
                                    <a href="buttons.php">Buttons</a>
                                </li>
                                <li>
                                    <a href="notifications.php">Notifications</a>
                                </li>
                                <li>
                                    <a href="typography.php">Typography</a>
                                </li>
                                <li>
                                    <a href="icons.php"> Icons</a>
                                </li>
                                <li>
                                    <a href="grid.php">Grid</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Company Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="add-car.php"><i class="fa fa-car fa-fw"></i> Add Car</a></li>
                                <li><a href="add-car-vendor.php"><i class="fa fa-user-plus fa-fw"></i> Add Car Vendor</a></li>
                                <li><a href="add-customer.php"><i class="fa fa-user fa-fw"></i> Add Customer </a></li>
                                <li><a href="add-employee.php"><i class="fa fa-user-plus fa-fw"></i> Add Employee </a></li>
                                <li><a href="add-flight.php"><i class="fa fa-plane fa-fw"></i> Add Flight </a></li>
                                <li><a href="add-hotels.php"><i class="fa fa-hotel fa-fw"></i> Add Hotel </a></li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="blank.php">Blank Page</a>
                                </li>
                                <li>
                                    <a href="login.php">Login Page</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>