<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('navigation.php'); ?>


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Booking</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add booking
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <form role="form" method="post" action="insert-booking.php">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>From Where</label>
                                            <input class="form-control" name="tourFromWhere" placeholder="Enter From Where place">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Select Date</label>
                                            <input class="form-control" type="date" name="fromDate" placeholder="Select Date">
                                        </div>

                                        <div class="form-group">
                                            <label>Price</label>
                                            <input class="form-control" name="price" placeholder="Enter tour price">
                                        </div>
                                      
                                        <div class="form-group">
                                            <label>Duration</label>
                                            <select name="duration" class="form-control">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Includes</label>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="Insurrance">
                                                            Insurrance
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="All drink included">
                                                            All drink included
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="Lunch in restaurant">
                                                                Lunch in restaurant
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="Tour guide">
                                                            Tour guide
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="Travel Insurance">
                                                            Travel Insurance
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="All tickeds museum">
                                                            All tickeds museum
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="Tour guide">
                                                            Conference Room
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="Travel Insurance">
                                                            Transfers
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="All tickeds museum">
                                                            Meals
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="Tour guide">
                                                            Sight Seeing
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="Travel Insurance">
                                                            Flight
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="Includes[]" value="All tickeds museum">
                                                            Budget Hotel
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary" name="submit" value="submit">Add Booking</button>
                                        <button type="reset" class="btn btn-default">Reset Booking</button>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>To Where</label>
                                            <input class="form-control" name="tourToWhere" placeholder="Enter To Where place">
                                        </div>
                                        <div class="form-group">
                                            <label>Select To Date</label>
                                            <input class="form-control" type="date" name="toDate" placeholder="Select to Date">
                                        </div>
                                        <div class="form-group">
                                            <label>Discount %</label>
                                            <input class="form-control" name="discount" placeholder="Enter Discout %">
                                        </div>
                                        <div class="form-group">
                                            <label>Tour Description</label>
                                            <textarea class="form-control" name="description"></textarea>
                                        </div>
                                    </div>
                                </form>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
