<div class="wrapper-mobile-nav">
                <div class="header-topbar">
                    <div class="topbar-search search-mobile">
                        <form class="search-form">
                            <div class="input-icon">
                                <i class="btn-search fa fa-search"></i>
                                <input type="text" placeholder="Search here..." class="form-control" />
                            </div>
                        </form>
                    </div>
                </div>
                <div class="header-main">
                    <div class="menu-mobile">
                        <ul class="nav-links nav navbar-nav">
                            <li class="dropdown">
                                <a href="index.php" class="main-menu">
                                    <span class="text">Home</span>
                                </a>
                                <span class="icons-dropdown">
                                    <i class="fa fa-angle-down"></i>
                                </span>
                                <ul class="dropdown-menu dropdown-menu-1">
                                    <li>
                                        <a href="index.php" class="link-page">Homepage default</a>
                                    </li>
                                    <li>
                                        <a href="homepage-02.php" class="link-page">Homepage 02</a>
                                    </li>
                                    <li>
                                        <a href="homepage-03.php" class="link-page">Homepage 03</a>
                                    </li>
                                    <li>
                                        <a href="homepage-04.php" class="link-page">Homepage 04</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="about-us.php" class="main-menu">
                                    <span class="text">about</span>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="tour-result.php" class="main-menu">
                                    <span class="text">Tour</span>
                                </a>
                                <span class="icons-dropdown">
                                    <i class="fa fa-angle-down"></i>
                                </span>
                                <ul class="dropdown-menu dropdown-menu-1">
                                    <li>
                                        <a href="tour-result.php" class="link-page">tour result</a>
                                    </li>
                                    <li>
                                        <a href="tour-view.php" class="link-page">tour view</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="hotel-result.php" class="main-menu">
                                    <span class="text">packages</span>
                                </a>
                                <span class="icons-dropdown">
                                    <i class="fa fa-angle-down"></i>
                                </span>
                                <ul class="dropdown-menu dropdown-menu-1">
                                    <li>
                                        <a href="hotel-result.php" class="link-page">hotel result</a>
                                    </li>
                                    <li>
                                        <a href="hotel-view.php" class="link-page">hotel view</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="blog.php" class="main-menu">
                                    <span class="text">blog</span>
                                </a>
                                <span class="icons-dropdown">
                                    <i class="fa fa-angle-down"></i>
                                </span>
                                <ul class="dropdown-menu dropdown-menu-1">
                                    <li>
                                        <a href="blog.php" class="link-page">blog list</a>
                                    </li>
                                    <li>
                                        <a href="blog-detail.php" class="link-page">blog detail</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="car-rent-result.php" class="main-menu">
                                    <span class="text">page</span>
                                </a>
                                <span class="icons-dropdown">
                                    <i class="fa fa-angle-down"></i>
                                </span>
                                <ul class="dropdown-menu dropdown-menu-1">
                                    <li>
                                        <a href="car-rent-result.php" class="link-page">car rent result</a>
                                    </li>
                                    <li>
                                        <a href="cruises-result.php" class="link-page">cruises result</a>
                                    </li>
                                    <li>
                                        <a href="404.php" class="link-page">page 404</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="contact.php" class="main-menu">
                                    <span class="text">contact</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline login-widget">
                            <li>
                                <a href="sign-in.php" class="item">login</a>
                            </li>
                            <li>
                                <a href="register.php" class="item">register</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>